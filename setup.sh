#!/bin/sh

# Makes build directory if it's not in current directory.
if [ ! -d build ]; then
  echo "Make build Directory."
  mkdir build
fi

# Move to build directory.
cd build

# Execute cmake with flag, if build directory is empty!!!
# TIP: Use 'rm -r build' to remove build and everything in it.
if [ $(find ../build -maxdepth 0 -type d -empty 2>/dev/null) ];then
  echo "Execute cmake."
  cmake -DCMAKE_BUILD_TYPE=Debug ..
fi

# Execute make install, makes executable in current directory (not in build).
make install

cd ..
cd bin
mv gradius ..
