var searchData=
[
  ['_5fdifficulty',['_difficulty',['../df/d4f/classutil_1_1Settings.html#a3667e9f19311126923880ef43bc68755',1,'util::Settings']]],
  ['_5ffirespeed',['_firespeed',['../df/d4f/classutil_1_1Settings.html#a42edb5d7efde277f827e7deaaa3f163f',1,'util::Settings']]],
  ['_5ffontfile',['_fontfile',['../df/d4f/classutil_1_1Settings.html#a7615545fb7352342ce92cdea35cbf69a',1,'util::Settings']]],
  ['_5finstance',['_instance',['../df/d4f/classutil_1_1Settings.html#a0f017555f994b89c13aafa12c85d3ad0',1,'util::Settings']]],
  ['_5fplayerhealth',['_playerhealth',['../df/d4f/classutil_1_1Settings.html#aa38d18ebe447f7ef7834b3c6c8621384',1,'util::Settings']]],
  ['_5fplayerspeed',['_playerspeed',['../df/d4f/classutil_1_1Settings.html#a9483ca29015ed236d3cf21f6dc621c2b',1,'util::Settings']]],
  ['_5ftexturefile',['_texturefile',['../df/d4f/classutil_1_1Settings.html#a398d994f0686070637ecfc6513577c12',1,'util::Settings']]],
  ['_5fxmax',['_xmax',['../df/d4f/classutil_1_1Settings.html#addb16a27834a138a0afeea736f0fbdb2',1,'util::Settings']]],
  ['_5fxres',['_xres',['../df/d4f/classutil_1_1Settings.html#a93534ad70baa607fad5d657a6351f0af',1,'util::Settings']]],
  ['_5fymax',['_ymax',['../df/d4f/classutil_1_1Settings.html#a484ceb853a00038b318ab79ba74b3f7c',1,'util::Settings']]],
  ['_5fyres',['_yres',['../df/d4f/classutil_1_1Settings.html#a518eed82e2e946842e7c02dc987c64cb',1,'util::Settings']]]
];
