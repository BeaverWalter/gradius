var searchData=
[
  ['obstacle',['Obstacle',['../d5/dbc/classgradius_1_1Obstacle.html',1,'gradius']]],
  ['obstacle',['Obstacle',['../d5/dbc/classgradius_1_1Obstacle.html#a0e4ab9340cf38795c5438eebd67c21b0',1,'gradius::Obstacle::Obstacle()'],['../d5/dbc/classgradius_1_1Obstacle.html#abe4d0f118bc8f6c148a1b6b8e4ee1631',1,'gradius::Obstacle::Obstacle(double x, double y)'],['../d5/dbc/classgradius_1_1Obstacle.html#acac51f77c221638a5adb234cc2b31795',1,'gradius::Obstacle::Obstacle(double x, double y, double xs, double ys)'],['../d5/dbc/classgradius_1_1Obstacle.html#a0d3aefbc319646349452f418b3a55fb9',1,'gradius::Obstacle::Obstacle(double x, double y, double xs, double ys, double speed)']]],
  ['operator_21_3d',['operator!=',['../d6/da1/classVector.html#a35e5f656a75a815a3a6d722cd6843f2b',1,'Vector']]],
  ['operator_2a',['operator*',['../d6/da1/classVector.html#ae2d1e0fa8feab25429528ceff5acdb38',1,'Vector::operator*(const Vector &amp;that) const'],['../d6/da1/classVector.html#a550d020054b19c7a0c23e426956fd370',1,'Vector::operator*(const int &amp;that) const']]],
  ['operator_2a_3d',['operator*=',['../d6/da1/classVector.html#a8529fe628a255d566c8e13c4567102b2',1,'Vector::operator*=(const Vector &amp;that)'],['../d6/da1/classVector.html#a2cbace501d137b1f6cf54386d9fca19e',1,'Vector::operator*=(const int &amp;that)']]],
  ['operator_2b',['operator+',['../d6/da1/classVector.html#a7eaa554b95984fb81951924e936e0fdc',1,'Vector']]],
  ['operator_2b_3d',['operator+=',['../d6/da1/classVector.html#a592fb8dd3235ab5e116148367ca873c1',1,'Vector']]],
  ['operator_2d',['operator-',['../d6/da1/classVector.html#a1836252b5ae0689f82369314ef3de7e3',1,'Vector']]],
  ['operator_2d_3d',['operator-=',['../d6/da1/classVector.html#a42a5115d3589f64d9b161a84d4b9750e',1,'Vector']]],
  ['operator_3d',['operator=',['../d6/da1/classVector.html#acc0576d51ee02fab00c0279a976d9ab3',1,'Vector']]],
  ['operator_3d_3d',['operator==',['../d6/da1/classVector.html#a29052b90f6b14f4eff05aa4008bbd7c1',1,'Vector']]]
];
