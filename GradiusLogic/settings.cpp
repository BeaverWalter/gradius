//
// Created by walter on 22.12.16.
//

#include "settings.hh"

using namespace std;
using namespace util;

namespace pt = boost::property_tree;

Settings* Settings::_instance = nullptr;

Settings::Settings() {

    _xres = 800;
    _yres = 600;

    _xmax = 4;
    _ymax = 3;

}

Settings* Settings::instance() {


    if (!_instance)   // Only allow one instance of class to be generated.
        _instance = new Settings;

    return _instance;

}

void Settings::readSettings(string filename) {

    pt::ptree settings;
    pt::read_json(filename, settings);

    _xres = settings.get<int>("settings.xres", 800);
    _yres = settings.get<int>("settings.yres", 600);

    _xmax = settings.get<int>("settings.xmax", 4);
    _ymax = settings.get<int>("settings.ymax", 3);

    _playerspeed = settings.get<double>("player.speed", 3.0);
    _playerhealth = settings.get<int>("player.health", 10);
    _firespeed = settings.get<double>("player.firespeed", 15.0);

    _fontfile = settings.get<string>("files.font", "src/fonts/PressStart2P.ttf");
    _texturefile = settings.get<string>("files.textures", "src/textures/spritesheet.png");

    _difficulty = settings.get<int>("settings.difficulty", 1);

    try {
        if (_difficulty == 0) {
            _difficulty = 1;
            throw domain_error("Difficulty cannot be ZERO!");
        }
    }
    catch (domain_error& e) {
        cerr << e.what();
    }


}

double Settings::getXMax() const {
    return _xmax;
}

double Settings::getYMax() const {
    return _ymax;
}

int Settings::getXRes() const {
    return _xres;
}

int Settings::getYRes() const {
    return _yres;
}

double Settings::getPlayerSpeed() const {
    return _playerspeed;
}

int Settings::getPlayerHealth() const {
    return _playerhealth;
}

double Settings::getFireSpeed() const {
    return _firespeed;
}

string Settings::getFontFile() const {
    return _fontfile;
}

string Settings::getTextureFile() const {
    return _texturefile;
}

int Settings::getDifficulty() const {
    return _difficulty;
}