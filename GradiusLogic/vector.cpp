#include "headers/vector.hh"

Vector::Vector(){

    xPos = 0;
    yPos = 0;

}

Vector::Vector(double x, double y){

    xPos = x;
    yPos = y;

}

Vector::~Vector() {}

double Vector::getX() const {

    return xPos;

}

double Vector::getY() const {

    return yPos;

}

void Vector::setPos(double x, double y) {

    xPos = x;
    yPos = y;

}

Vector Vector::operator=(const Vector& that){

    if (this != &that) {
        this->setPos(that.getX(),that.getY());
    }
    return *this;

}



Vector Vector::operator+(const Vector &that) const {

    return(Vector(this->getX() + that.getX(), this->getY() + that.getY()));

}

Vector Vector::operator-(const Vector &that) const {

    return(Vector(this->getX() - that.getX(), this->getY() - that.getY()));

}

Vector Vector::operator*(const Vector &that) const {

    return(Vector(this->getX() * that.getX(), this->getY() * that.getY()));

}



Vector Vector::operator*(const int &that) const {

    return(Vector(this->getX() * that, this->getY() * that));

}



Vector Vector::operator+=(const Vector &that) {

    this->setPos(this->getX() + that.getX(), this->getY() + that.getY());
    return *this;

}

Vector Vector::operator-=(const Vector &that) {

    this->setPos(this->getX() - that.getX(), this->getY() - that.getY());
    return *this;

}

Vector Vector::operator*=(const Vector &that) {

    this->setPos(this->getX() * that.getX(), this->getY() * that.getY());
    return *this;

}



Vector Vector::operator*=(const int &that) {

    this->setPos(this->getX() * that, this->getY() * that);
    return *this;

}



bool Vector::operator==(const Vector& that){

    return (this->getX() == that.getX() and this->getY() == that.getY());

}

bool Vector::operator!=(const Vector& that){

    return !(this->getX() == that.getX() and this->getY() == that.getY());

}

double Vector::dot(Vector &that) const {

    return(this->getX()*that.getX() + this->getY()*that.getY());

}

ostream& operator<< (ostream& output, const Vector& vector) {

    output << "(" << vector.getX() << ", " << vector.getY() << ")" << endl;
    return output;

}

