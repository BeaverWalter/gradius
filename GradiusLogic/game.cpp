//
// Created by walter on 22.11.17.
//

#include "game.hh"

namespace pt = boost::property_tree;

using namespace gradius;

Game::Game() {}

bool Game::getFirstLoad() const {
    return _firstload;
}

void Game::setFirstLoad(bool fl) {
    _firstload = fl;
}

void Game::reloadGame() {
    _levelindex = 0;
    _score = 0;
    _firstload = true;
    _endgame = false;
}

int Game::getKills() const {
    return _kills;
}

void Game::setKills(int kills) {
    _kills = kills;
}

void Game::readLevels() {

    _levelindex = 0;
    _score = 0;

    pt::ptree levels;
    pt::read_json("src/levels/levels.json", levels);

    vector<string> levellist;

    for (pt::ptree::value_type& level: levels.get_child("files"))
    {
        string levelname = level.second.data();
        levellist.push_back(levelname);
    }

    _levellist = levellist;

}

bool Game::loadNextLevel() {

    if (_levelindex < _levellist.size()) {

        pt::ptree level;
        pt::read_json(_levellist[_levelindex], level);

        _enemylimit = level.get<int>("enemylimit", 50);

        _enemychance = level.get<int>("enemychance", 1);
        _enemycooldown = level.get<double>("enemycooldown", 10.0);
        _enemyswitchchance = level.get<int>("enemyswitchchance", 2);

        _obstaclechance = level.get<int>("obstaclechance", 1);
        _obstaclecooldown = level.get<double>("obstaclecooldown", 10.0);

        _kills = 0;

        _levelindex++;
    }

    else if (_levelindex >= _levellist.size()) {
        _endgame = true;
    }
}

double Game::getEnemyCooldown() const {
    return _enemycooldown;
}

int Game::getEnemyChance() const {
    return _enemychance;
}

int Game::getEnemySwitchChance() const {
    return _enemyswitchchance;
}

double Game::getObstacleCooldown() const {
    return _obstaclecooldown;
}

int Game::getObstacleChance() const {
    return _obstaclechance;
}

bool Game::testLevelEnd() {
    if (_levelindex >= _levellist.size() && _kills == _enemylimit) {
        _endgame = true;
    }
    return (_kills >= _enemylimit);
}

int Game::getScore() const {
    return _score;
}

void Game::setScore(int score) {
    _score = score;
}

bool Game::getEndGame() const {
    return _endgame;
}

void Game::setEndGame(bool end) {
    _endgame = end;
}
