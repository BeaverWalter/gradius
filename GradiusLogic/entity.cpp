//
// Created by walter on 13.11.17.
//

#include "entity.hh"

using namespace gradius;

Entity::Entity(double x, double y, double xs, double ys, double speed) {

    _location.setPos(x, y);
    _size.setPos(xs, ys);
    _speed = speed;

}

Vector Entity::getLocation() const {

    return _location;

}

Vector Entity::getSize() const {

    return _size;

}

double Entity::getSpeed() const {

    return _speed;

}

void Entity::setLocation(double xp, double yp) {

    _location.setPos(xp, yp);

}

void Entity::setLocation(Vector loc) {

    _location = loc;

}

void Entity::setCenterLocation(Vector& loc) {

    Vector destination(loc.getX() - this->getSize().getX(), loc.getY() - this->getSize().getY());
    _location = destination;

}

void Entity::setSize(double xs, double ys) {

    _size.setPos(xs, ys);

}

void Entity::setSpeed(double speed) {

    _speed = speed;

}