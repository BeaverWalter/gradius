//
// Created by walter on 22.12.16.
//

#include <iostream>
#include "random.hh"

using namespace util;

Random* Random::m_pInstance = nullptr;

Random* Random::instance() {


    if (!m_pInstance)   // Only allow one instance of class to be generated.
        m_pInstance = new Random;

    return m_pInstance;

}

Random::Random() {
    srand(time(NULL));
}

int Random::randomInt() const {

    int r = rand() % _range;
    return r;

}