//
// Created by walter on 17.11.17.
//

#include "obstacle.hh"

using namespace gradius;

void Obstacle::configure(bool top, int type) {

    _top = top;
    _type = (Obstacle::Type)type;

}

bool Obstacle::getTop() const {
    return _top;
}

gradius::Obstacle::Type Obstacle::getType() const {
    return _type;
}