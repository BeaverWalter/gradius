//
// Created by walter on 13.11.17.
//

#include "chronometer.hh"

using namespace util;

Chronometer::Chronometer() {

    _time = _clock.now();

}

double Chronometer::deltaTime() const {

    return _deltaTime;

}

void Chronometer::resetClock() {

    chrono::duration<float> difference = _clock.now() - _time;
    _time = _clock.now();
    _deltaTime = difference.count();

}
