//
// Created by walter on 13.11.17.
//

#include "world.hh"

using namespace gradius;

World::~World() {}

World::World(double x, double y, double xs, double yx, double speed) : Entity(x, y, xs, yx, speed) {

    gradius::Player* player = new Player(0, 0, 0.4, 0.4, util::Settings::instance()->getPlayerSpeed());
    player->configure(util::Settings::instance()->getPlayerHealth());

    gradius::Background* background = new Background(-4, -3, 16, 6, 0.5);

    gradius::Wall* walltop = new Wall(-4, -3, 16, 0.4, 1);
    walltop->configure(true);
    gradius::Wall* wallbottom = new Wall(-4, 3 - 0.4, 16, 0.4, 1);
    pair<gradius::Wall*, gradius::Wall*> walls = make_pair(walltop, wallbottom);

    this->configure(player, background, walls, true);

}

void World::configure(Player *player, Background* background, pair<Wall*, Wall*> walls, bool pause) {

    _player = shared_ptr<Player>(player);
    _background = shared_ptr<Background>(background);
    _walls.first = shared_ptr<Wall>(walls.first);
    _walls.second = shared_ptr<Wall>(walls.second);
    _state.paused = pause;

    _state.paused = true;

}

World::Gamestate* World::getGamestate() {
    return &_state;
}

shared_ptr<Player> World::getPlayer() const {
    return _player;
}

shared_ptr<Background> World::getBackground() const {
    return _background;
}

pair<shared_ptr<Wall>, shared_ptr<Wall>> World::getWalls() const {
    return _walls;
}

const vector<shared_ptr<Missile>>& World::getMissiles() const {
    return _missiles;
}

const vector<shared_ptr<Obstacle>>& World::getObstacles() const {
    return _obstacles;
}

const vector<shared_ptr<Enemy>>& World::getEnemies() const {
    return _enemies;
}


void World::setObstacleCooldown(double cooldown) {
    _obstaclecooldown = cooldown;
}

double World::getObstacleCooldown() {
    return _obstaclecooldown;
}


void World::setEnemyCooldown(double cooldown) {
    _enemycooldown = cooldown;
}

double World::getEnemyCooldown() {
    return _enemycooldown;
}

//missile list control
void World::addMissile(shared_ptr<Missile> missile) {
    _missiles.push_back(missile);
}

void World::setMissile(shared_ptr<Missile> missile, int index) {
    _missiles[index] = missile;
}

void World::deleteMissile(int index) {
    _missiles[index].reset();
}

void World::setMissiles(vector<shared_ptr<Missile>> mlist) {
    _missiles = mlist;
}

//obstacle list control
void World::addObstacle(shared_ptr<Obstacle> obstacle) {
    _obstacles.push_back(obstacle);
}

void World::setObstacle(shared_ptr<Obstacle> obstacle, int index) {
    _obstacles[index] = obstacle;
}

void World::deleteObstacle(int index) {
    _obstacles[index].reset();
}

void World::setObstacles(vector<shared_ptr<Obstacle>> olist) {
    _obstacles = olist;
}


//enemy list control
void World::addEnemy(shared_ptr<Enemy> enemy) {
    _enemies.push_back(std::move(enemy));
}

void World::setEnemy(shared_ptr<Enemy> enemy, int index) {
    _enemies[index] = enemy;
}

void World::deleteEnemy(int index) {
    _enemies[index].reset();
}

void World::setEnemies(vector<shared_ptr<Enemy>> elist) {
    _enemies = elist;
}


void World::setPause(bool pause) {
    _state.paused = pause;
}

void World::setSoftReset(bool first) {
    if(first)
        _player->setHealth(util::Settings::instance()->getPlayerHealth());
    _player->setLocation(0, 0);
    _player->setHitCooldown(10.0);

    for (int d = 0; d < _missiles.size(); d++) {
        if (_missiles[d] != nullptr) {
            _missiles[d] = nullptr;
        }
    }
    _missiles = {};

    for (int d = 0; d < _enemies.size(); d++) {
        if (_enemies[d] != nullptr) {
            _enemies[d] = nullptr;
        }
    }
    _enemies = {};

    for (int d = 0; d < _obstacles.size(); d++) {
        if (_obstacles[d] != nullptr) {
            _obstacles[d] = nullptr;
        }
    }
    _obstacles = {};
}