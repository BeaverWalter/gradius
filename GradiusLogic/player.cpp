//
// Created by walter on 13.11.17.
//

#include "player.hh"

using namespace gradius;

Player::Player(double x, double y, double xs, double ys, double speed) : Entity(x, y, xs, ys, speed) {

    _firecooldown = 10;
    _hitcooldown = 10;

}

void Player::configure(int health) {
    _health = health;
}

void Player::setFireCooldown(double cd) {
    _firecooldown = cd;
}

void Player::setHitCooldown(double cd) {
    _hitcooldown = cd;
}

void Player::setHealth(int health) {
    _health = health;
}

double Player::getFireCooldown() const{
    return _firecooldown;
}

double Player::getHitCooldown() const{
    return _hitcooldown;
}

int Player::getHealth() const {
    return _health;
}

bool Player::checkColOut(Entity* entity) {

    Vector pos = this->getLocation();
    Vector collider = entity->getLocation();

    Vector size = this->getSize();
    double w = entity->getSize().getX();
    double h = entity->getSize().getY();

    if (pos.getX() < collider.getX() + w &&
        pos.getX() + size.getX() > collider.getX() &&
        pos.getY() < collider.getY() + h &&
        pos.getY() + size.getY() > collider.getY()) {

        return true;
    }

    return false;
}