#ifndef VECTOR_HH
#define VECTOR_HH

#include <iostream>


using namespace std;

    /**
     *
     * VECTOR Class\n
     *
     * Basic 'position' class used by entities\n
     * Note: all overloaded operator have 'common uses' (addition, multiplication...)  uneless specified otherwise
     *
     */
    class Vector{

    public:

        /**
         *
         * Basic constructor for Vector\n
         * @param NONE
         *
         */
        Vector();
        /**
         *
         * Constructor for Vector\n
         * Sets the first (x) and second (y) position of the vector\n
         *
         * @param x (double) first position
         * @param y (double) second position
         */
        Vector(double x, double y);

        /**
         *
         * Virtual destructor of Vector\n
         *
         */
        virtual ~Vector();

        /**
         *
         * Function that returns the position on the x axis
         *
         * @return
         */
        double getX() const;
        /**
         *
         * Function that returns the position on the y axis
         *
         * @return
         */
        double getY() const;

        /**
         *
         * Function that changes both positions of the vector (x and y)\n
         *
         * @param x (double) first location (x axis)
         * @param y (double) second location (y axis)
         */
        void setPos(double x, double y);

        /**
         *
         * Assignment operator\n
         *
         * @param that (Arkanoid::Vector)
         * @return
         */
        Vector operator=(const Vector& that);

        /**
         *
         *  + operator\n
         *
         * @param that (Arkanoid::Vector)
         * @return
         */
        Vector operator+(const Vector& that) const;
        /**
         *
         *  - operator\n
         *
         * @param that (Arkanoid::Vector)
         * @return
         */
        Vector operator-(const Vector& that) const;
        /**
         *
         *  * operator with another vector
         *
         * @param that (Arkanoid::Vector)
         * @return
         */
        Vector operator*(const Vector& that) const;

        /**
         *
         *  * operator with an int\n
         *
         * @param that (int)
         * @return
         */
        Vector operator*(const int& that) const;

        /**
         *
         *  += operator\n
         *
         * @param that (Arkanoid::Vector)
         * @return
         */
        Vector operator+=(const Vector& that);
        /**
         *
         *  -= operator\n
         *
         * @param that (Arkanoid::Vector)
         * @return
         */
        Vector operator-=(const Vector& that);
        /**
         *
         *  *= operator with another vector\n
         *
         * @param that (Arkanoid::Vector)
         * @return
         */
        Vector operator*=(const Vector& that);

        /**
         *
         *  *= operator with an int\n
         *
         * @param that (int)
         * @return
         */
        Vector operator*=(const int& that);

        /**
         *
         *  != operator\n
         *
         * @param that (Arkanoid::Vector)
         * @return
         */
        bool operator!=(const Vector& that);
        /**
         *
         *  == operator\n
         *
         * @param that (Arkanoid::Vector)
         * @return
         */
        bool operator==(const Vector& that);

        /**
         *
         * Dot multiplication, returns a double
         *
         * @param that (Arkanoid::Vector)
         * @return double
         */
        double dot(Vector& that) const;

        friend std::ostream& operator<< (std::ostream& stream, const Vector& vector);

    private:

        double xPos; /**< position of the vector on the x axis */
        double yPos; /**< position of the vector on the y axis */
    };


#endif /*VECTOR_HH*/