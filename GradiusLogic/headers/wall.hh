//
// Created by walter on 17.11.17.
//

#ifndef GRADIUS_WALL_H
#define GRADIUS_WALL_H

#include "entity.hh"

namespace gradius {

    /**
     *
     * GRADIUS::WALL Class\n
     * The class that manages all wall specific details\n
     *
     */
    class Wall : public Entity {

        bool _top = false; /**< boolean that is true when a wall object is a 'top' wall object */

    public:

        /**
         * Default constructor for the Wall class
         */
        Wall() : Entity(0, 0, 1, 1){}
        /**
         * Constructor for the Wall class that sets the position
         * @param x
         * @param y
         */
        Wall(double x, double y) : Entity(x, y, 1, 1) {}
        /**
         * Constructor for the Wall class that sets the position and the size
         * @param x
         * @param y
         * @param xs
         * @param ys
         */
        Wall(double x, double y, double xs, double ys) : Entity(x, y, xs, ys, 1.0) {}
        /**
         * Constructor for the Wall class that sets the position, the size and the speed
         * @param x
         * @param y
         * @param xs
         * @param ys
         * @param speed
         */
        Wall(double x, double y, double xs, double ys, double speed) : Entity(x, y, xs, ys, speed){}

        /**
         * Virtual default destructor for the Wall class
         */
        virtual ~Wall() = default;

        /**
         * Function that configures additional parameters for the wall
         * @param top
         */
        void configure(bool top);

        /**
         * Function that returns if a wall is top or not
         * @return
         */
        bool getTop() const;

    };

}

#endif //GRADIUS_WALL_H
