//
// Created by walter on 16.11.17.
//

#ifndef GRADIUS_MISSILE_H
#define GRADIUS_MISSILE_H

#include "entity.hh"

namespace gradius {

    /**
     *
     * GRADIUS::MISSILE Class\n
     * The class that manages all missile specific details\n
     *
     */
    class Missile : public Entity {

    public:

        /**
         * Default constructor for the Missile class
         */
        Missile() : Entity(0, 0, 1, 1){}
        /**
         * Constructor for the Missile class that sets the position
         * @param x
         * @param y
         */
        Missile(double x, double y) : Entity(x, y, 1, 1) {}
        /**
         * Constructor for the Missile class that sets the position and the size
         * @param x
         * @param y
         * @param xs
         * @param ys
         */
        Missile(double x, double y, double xs, double ys) : Entity(x, y, xs, ys, 1.0) {}
        /**
         * Constructor for the Missile class that sets the position, the size and the speed
         * @param x
         * @param y
         * @param xs
         * @param ys
         * @param speed
         */
        Missile(double x, double y, double xs, double ys, double speed) : Entity(x, y, xs, ys, speed){}

        /**
         * Virtual desctructor for the Missile class
         */
        virtual ~Missile() = default;

        /**
         * Collision function that checks if the missile goes outside of an object (thus: Check Collision with Inside of object)
         * @param entity
         * @return
         */
        bool checkColIn(Entity* entity);
        /**
         * Collision function that checks if the missile collides with an object (thus: Check Collision with outside of object)
         * @param entity
         * @return
         */
        bool checkColOut(Entity* entity);

    };

}

#endif //GRADIUS_MISSILE_H
