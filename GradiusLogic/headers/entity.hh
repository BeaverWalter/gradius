//
// Created by walter on 13.11.17.
//

#ifndef GRADIUS_ENTITY_H
#define GRADIUS_ENTITY_H

#include <memory>
#include <string>

#include "vector.hh"

namespace gradius {

    /**
     *
     * GRADIUS::ENTITY Class\n
     * Is the base class for all "thing" in the game\n
     *
     */
    class Entity {

        Vector _location; /**< the location of the entity */
        Vector _size; /**< the size of the entity */
        double _speed; /**< the speed of an entity */

    public:

        /**
         * Virtual default constructor for the Entity Class
         */
        virtual ~Entity() = default;

        /**
         * Default constructor for the Entity class
         */
        Entity() : Entity(0, 0, 1.0, 1.0, 1.0) {}
        /**
         * Constructor for the Entity class that sets the position
         * @param x
         * @param y
         */
        Entity(double x, double y) : Entity(x, y, 1.0, 1.0, 1.0) {}
        /**
         * Constructor for the Entity class that sets the position and the size
         * @param x
         * @param y
         * @param xs
         * @param ys
         */
        Entity(double x, double y, double xs, double ys) : Entity(x, y, xs, ys, 1.0) {}
        /**
         * Constructor for the Entity class that sets the position, the size and the speed
         * @param x
         * @param y
         * @param xs
         * @param ys
         * @param speed
         */
        Entity(double x, double y, double xs, double ys, double speed);

        /**
         * Function that returns the location of the entity
         * @return
         */
        Vector getLocation() const;
        /**
         * Function that returns the size of the entity
         * @return
         */
        Vector getSize() const;
        /**
         * Function that returns the speed of the entity
         * @return
         */
        double getSpeed() const;

        /**
         * Function that sets the location of the entity (using doubles)
         * @param xp
         * @param yp
         */
        void setLocation(double xp, double yp);
        /**
         * Function that sets the location of the entity (using a vector)
         * @param loc
         */
        void setLocation(Vector loc);
        /**
         * Function that sets the location of the entity using the center of the entity, instead of the upper left corner
         * @param loc
         */
        void setCenterLocation(Vector& loc);

        /**
         * Function that sets the size of the entity
         * @param xs
         * @param ys
         */
        void setSize(double xs, double ys);
        /**
         * Function that sets the speed of the entity
         * @param speed
         */
        void setSpeed(double speed);

    };

}

#endif //GRADIUS_ENTITY_H
