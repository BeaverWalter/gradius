//
// Created by walter on 07.12.17.
//

#ifndef SINGLETON_H
#define SINGLETON_H

namespace util {

    /**
     * UTIL::SINGLETON Template Class\n
     * Template that can be used to make a template of any class\n
     * @tparam T
     */
    template<typename T>
    class Singleton {

    private:
        static T* _tinstance; /**< static instance of the class T */
        inline explicit Singleton<T>() = default; /**< default constructor of the singleton class that must be private  */

    public:
        /**
         * Function that returns the static instance of the class, or creates a new one if there isn't one yet
         * @return
         */
        static T* instance() {
            if(!Singleton::_tinstance)
                Singleton::_tinstance = new T();

            return Singleton::_tinstance;
        }

    };

    template <typename T>
    T* Singleton<T>::_tinstance = 0;

};



#endif //SINGLETON_H
