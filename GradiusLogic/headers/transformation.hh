#ifndef TRANSFORMATION_H
#define TRANSFORMATION_H

#include "vector.hh"
#include <cmath>

namespace util {

    /**
     *
     * UTIL::TRANSFORMATION Class\n
     * Class that can transform coordinates in a given coordinate system, to a given screensize\n
     *
     */
    class Transformation {

    public:

        /**
         *
         * Basic constructor for Transformation
         *
         */
        Transformation();

        /**
         *
         * Constructor for Transformation\n
         * Sets the width and height for the transformation\n
         *
         * @param x
         * @param y
         */
        Transformation(int x, int y);

        /**
         *
         * Function that sets a width and a height to be used by the transormationfunction itself
         *
         * @param width (int
         * @param height (int)
         */
        void setTrans(int width, int height);

        /**
         *
         * Function that applies the transformation on a vector\n
         * This vector can be a location or a size (this is indicated by the loc bool)\n
         *
         * @param vec
         * @param loc
         * @return
         */
        Vector transform(Vector vec, bool loc = true) const;

        /**
         * Function that sets the coordinate system upon which the transformation acts\n
         *
         * @param xm
         * @param ym
         */
        void setMax(double xm, double ym);

    private:

        int width; /**< width of the transformed x axis */
        int height; /**< height of the transformed y axis */

        double _xmax; /**< total width / 2 of the coordinate system */
        double _ymax; /**< total height / 2 of the coordinate system */

    };

};


#endif //TRANSFORMATION_H
