//
// Created by walter on 20.11.17.
//

#ifndef GRADIUS_ENEMY_H
#define GRADIUS_ENEMY_H

#include "entity.hh"

namespace gradius {

    /**
     *
     * GRADIUS::ENEMY Class\n
     * The class that manages all enemy specific details\n
     *
     */
    class Enemy : public Entity {

        int _health; /**< the value that keeps hold of the enemy health */
        double _offset; /**< the value that keeps the initial given offset (used for positioning) of the enemy */
        double _followspeed;

        enum Type {
            normal = 0,
            sick = 1,
            speedy = 2,
            heavy = 3
        } _type; /**< enum that keeps track of the type of the enemy */

    public:

        /**
         * Default constructor for the Enemy class
         */
        Enemy() : Entity(0, 0, 1.0, 1.0, 1.0){}
        /**
         * Constructor for the Enemy class that sets the position
         * @param x
         * @param y
         */
        Enemy(double x, double y) : Entity(x, y, 1.0, 1.0, 1.0) {}
        /**
         * Constructor for the Enemy class that sets the position and the size
         * @param x
         * @param y
         * @param xs
         * @param ys
         */
        Enemy(double x, double y, double xs, double ys) : Entity(x, y, xs, ys, 1.0) {}
        /**
         * Constructor for the Enemy class that sets the postition, the size and the speed
         * @param x
         * @param y
         * @param xs
         * @param ys
         * @param speed
         */
        Enemy(double x, double y, double xs, double ys, double speed) : Entity(x, y, xs, ys, speed) {}

        /**
         * Virtual default constructor of the Enemy class
         */
        virtual ~Enemy() = default;

        /**
         * Function to configure additional details for an enemy
         * @param type
         * @param health
         * @param offset
         */
        void configure(int type = 0, int health = 1, double offset = 0, double followSpeed = 0.5);

        /**
         * Function that sets the health of the enemy
         * @param health
         */
        void setHealth(int health);
        /**
         * Function that returns the (current) health of an enemy
         * @return
         */
        int getHealth() const;

        /**
         * Function that returns the (positional) offset of an enemy
         * @return
         */
        double getOffset() const;

        /**
         * Function that returns the followspeed of an enemy
         * @return
         */
        double getFollowSpeed() const;

        /**
         * Function that returns the type of the enemy
         * @return
         */
        Enemy::Type getType() const;

        /**
         * Collision function that checks if an enemy collides with another object (Check Collision with Outside of Object)
         * @param entity
         * @return
         */
        bool checkColOut(Entity* entity);
    };

}

#endif //GRADIUS_ENEMY_H
