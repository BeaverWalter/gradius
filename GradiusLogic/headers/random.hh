//
// Created by walter on 22.12.16.
//

#ifndef RANDOM_H
#define RANDOM_H

#include <cstdlib>
#include <time.h>

namespace util {

    /**
     *
     * UTIL::RANDOM Class\n
     * Class that gives a random integer between 0 and 99999999
     *
     */
    class Random {

    public:

        /**
         *
         * Function that creates an instance of the transformation if none exists\n
         *
         * @return
         */
        static Random *instance();

        /**
         *
         * Function that generates a random integer between 0 and 999\n
         *
         */
        int randomInt() const;

    private:

        /**
         *
         * Basic constructor for Transformation
         *
         */
        Random();

        static Random *m_pInstance; /**< instance pointer of the transformation */

        int _range = 1000000000; /**< maximum of the random generated int */

    };

}

#endif //_RANDOM_H
