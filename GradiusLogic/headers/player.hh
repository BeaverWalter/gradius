//
// Created by walter on 13.11.17.
//

#ifndef GRADIUS_PLAYER_H
#define GRADIUS_PLAYER_H

#include "entity.hh"

namespace gradius {

    /**
     *
     * GRADIUS::PLAYER Class\n
     * The class that manages all player specific details\n
     *
     */
    class Player : public Entity {

        int _health; /**< (current) health of the player */
        string _name; /**< name of the player */
        double _firecooldown = 10.0; /**< (current) firecooldown of the player */
        double _hitcooldown = 10.0; /**< (current) collisioncooldown of the player */

    public:

        /**
         * Default constructor for the Player class
         */
        Player() : Entity(0, 0, 1.0, 1.0, 1.0){}
        /**
         * Constructor for the Player class that sets the position
         * @param x
         * @param y
         */
        Player(double x, double y) : Entity(x, y, 1.0, 1.0, 1.0) {}
        /**
         * Constructor for the Player class that sets the position and the size
         * @param x
         * @param y
         * @param xs
         * @param ys
         */
        Player(double x, double y, double xs, double ys) : Entity(x, y, xs, ys, 1.0) {}
        /**
         * Constructor for the Player class that sets the position, size and speed
         * @param x
         * @param y
         * @param xs
         * @param ys
         * @param speed
         */
        Player(double x, double y, double xs, double ys, double speed);

        /**
         * Virtual default destructor
         */
        virtual ~Player() = default;

        /**
         * Function to configure additional parameters for a player
         * @param health
         */
        void configure(int health);

        /**
         * Function that sets the _firecooldown
         * @param cd
         */
        void setFireCooldown(double cd);
        /**
         * Function that sets the _hitcooldown
         * @param cd
         */
        void setHitCooldown(double cd);
        /**
         * Function that sets the player health
         * @param health
         */
        void setHealth(int health);

        /**
         * Function that returns the _firecooldown value
         * @return
         */
        double getFireCooldown() const;
        /**
         * Function that returns the _hitcooldown value
         * @return
         */
        double getHitCooldown() const;
        /**
         * Function that returns the health of the player
         * @return
         */
        int getHealth() const;

        /**
         * Collision function that checks if the player collides with an object (Check Collision with Outside of Object)
         * @param entity
         * @return
         */
        bool checkColOut(Entity* entity);
    };

}

#endif //GRADIUS_PLAYER_H
