//
// Created by walter on 22.12.16.
//

#ifndef SETTINGS_H
#define SETTINGS_H

#include <cstdlib>
#include "iostream"
#include "fstream"
#include "vector"
#include "tuple"
#include "algorithm"
#include "exception"

#include "boost/property_tree/ptree.hpp"
#include "boost/property_tree/json_parser.hpp"

using namespace std;

namespace util {
    /**
     *
     * UTIL::SETTINGS Class\n
     *
     * Class that reads and holds all settings for the game itself (not levels)
     *
     */
    class Settings {

    public:

        /**
         *
         * Function that creates an instance of the transformation if none exists\n
         *
         * @return
         */
        static Settings* instance();

        /**
         *
         * Function that reads all the settings from a given file\n
         *
         * @return
         */
        void readSettings(string filename);

        /**
         * Function that returns the _xmax value
         * @return
         */
        double getXMax() const;
        /**
         * Function that returns the _ymax value
         * @return
         */
        double getYMax() const;

        /**
         * Function that returns the _xres value
         * @return
         */
        int getXRes() const;
        /**
         * Function that returns the _yres value
         * @return
         */
        int getYRes() const;

        /**
         * Function that returns the player speed
         * @return
         */
        double getPlayerSpeed() const;
        /**
         * Function that returns the player health (initial value)
         * @return
         */
        int getPlayerHealth() const;
        /**
         * Function that returns the fire speed (for the player)
         * @return
         */
        double getFireSpeed() const;

        /**
         * Function that returns the path to the font file
         * @return
         */
        string getFontFile() const;
        /**
         * Function that returns the path to the texture file
         * @return
         */
        string getTextureFile() const;

        /**
         * Function that returns the difficulty
         * @return
         */
        int getDifficulty() const;


    protected:

        /**
         *
         * Basic constructor for Settings
         *
         */
        Settings();

        static Settings* _instance; /**< instance pointer of the settings */

        //resolution
        int _xres; /**< x resolution of the game window */
        int _yres; /**< y resolution of the game window */

        //gamesize
        double _xmax; /**< width of the game coordinates (/2) */
        double _ymax; /**< height of the game coordinates (/2) */

        //player parameters
        double _playerspeed; /**< speed of the player */
        int _playerhealth; /**< initial health of the player */
        double _firespeed; /**< fireing speed of the player */

        //file parameters
        string _fontfile; /**< path to the fontfile */
        string _texturefile; /**< path to the texture file */

        //game parameters
        int _difficulty; /**< difficulty of the game */



    };
}


#endif //_SETTINGS_H
