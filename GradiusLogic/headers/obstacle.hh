//
// Created by walter on 17.11.17.
//

#ifndef GRADIUS_OBSTACLE_H
#define GRADIUS_OBSTACLE_H

#include "entity.hh"

namespace gradius {

    /**
     *
     * GRADIUS::ENTITY Class\n
     * The class that manages all player specific details;
     *
     */
    class Obstacle : public Entity {

        bool _top = false; /**< boolean that is true when the obstacle is a top obstacle */

        enum Type {
            rough = 0,
            bold = 1
        } _type; /**< enum which states the type of the obstacle (used for selecting a texture) */

    public:

        /**
         * Default constructor for the Obstacle class
         */
        Obstacle() : Entity(0, 0, 1, 1){}
        /**
         * Constructor for the Obstacle class that sets the position
         * @param x
         * @param y
         */
        Obstacle(double x, double y) : Entity(x, y, 1, 1) {}
        /**
         * Constructor for the Obstacle class that sets the position and the size
         * @param x
         * @param y
         * @param xs
         * @param ys
         */
        Obstacle(double x, double y, double xs, double ys) : Entity(x, y, xs, ys, 1.0) {}
        /**
         * Constructor for the Obstacle class that sets the position, size and speed
         * @param x
         * @param y
         * @param xs
         * @param ys
         * @param speed
         */
        Obstacle(double x, double y, double xs, double ys, double speed) : Entity(x, y, xs, ys, speed){}

        /**
         * Virtual default destructor
         */
        virtual ~Obstacle() = default;

        /**
         * Function that configures extra parameters for the obstacle
         * @param top
         * @param type
         */
        void configure(bool top, int type);

        /**
         * Function that returns if the obstacle is a top obstacle
         * @return
         */
        bool getTop() const;
        /**
         * Function that returns the type of the obstacle
         * @return
         */
        Obstacle::Type getType() const;

    };

}

#endif //GRADIUS_OBSTACLE_H
