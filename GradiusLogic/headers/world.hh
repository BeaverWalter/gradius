//
// Created by walter on 13.11.17.
//

#ifndef GRADIUS_WORLD_H
#define GRADIUS_WORLD_H

#include <vector>
#include <utility>

#include <transformation.hh>
#include <settings.hh>

#include <player.hh>
#include <background.hh>
#include <missile.hh>
#include <wall.hh>
#include <obstacle.hh>
#include <enemy.hh>

namespace gradius {

    /**
     *
     * GRADIUS::WORLD Class\n
     * Provides the interface and logic to the whole world\n
     *
     */
    class World : public Entity {

        struct Gamestate {
            bool paused;
        } _state; /**< struct that keeps track of the state(s) of the world */

        double _obstaclecooldown = 10.0; /**< value that keeps track of the (current) obstacle cooldown */
        double _enemycooldown = 10.0; /**< value that keeps track of the (current) enemy cooldown */

        shared_ptr<Player> _player; /**< the player of the world */
        shared_ptr<Background> _background; /**< the background of the world */
        pair<shared_ptr<Wall>, shared_ptr<Wall>> _walls; /**< the walls of the world */
        vector<shared_ptr<Missile>> _missiles; /**< the missiles of the world */
        vector<shared_ptr<Obstacle>> _obstacles; /**< the obstacles of the world */
        vector<shared_ptr<Enemy>> _enemies; /**< the enemies of the world */

    public:

        /**
         * Default constructor for the World class
         */
        World() : Entity(0, 0, 1, 1){}
        /**
         * Constructor for the World class that sets the position
         * @param x
         * @param y
         */
        World(double x, double y) : Entity(x, y, 1, 1) {}
        /**
         * Constuctor for the World class that sets the position and the size
         * @param x
         * @param y
         * @param xs
         * @param ys
         */
        World(double x, double y, double xs, double ys) : Entity(x, y, xs, ys, 1.0) {}
        /**
         * Constructor for the World class that sets the position, size and the speed
         * @param x
         * @param y
         * @param xs
         * @param ys
         * @param speed
         */
        World(double x, double y, double xs, double ys, double speed);

        /**
         * Virtual destructor for the World class
         */
        virtual ~World();

        /**
         * Function that configures additional parameters of the world
         * @param player
         * @param background
         * @param walls
         * @param pause
         */
        void configure(Player* player, Background* background, pair<Wall*, Wall*> walls, bool pause);

        /**
         * Function that returns the galestate(s)
         * @return
         */
        Gamestate* getGamestate();
        /**
         * Function that returns (a pointer to) the player
         * @return
         */
        shared_ptr<Player> getPlayer() const;
        /**
         * Function that returns (a pointer to) the background
         * @return
         */
        shared_ptr<Background> getBackground() const;
        /**
         * Function that returns the walls (as pointers)
         * @return
         */
        pair<shared_ptr<Wall>, shared_ptr<Wall>> getWalls() const;
        /**
         * Function that returns the missiles (as pointers)
         * @return
         */
        const vector<shared_ptr<Missile>>& getMissiles() const;
        /**
         * Function that returns the obstacles (as pointers)
         * @return
         */
        const vector<shared_ptr<Obstacle>>& getObstacles() const;
        /**
         * Function that returns the enemies (as pointers)
         * @return
         */
        const vector<shared_ptr<Enemy>>& getEnemies() const;

        /**
         * Function that sets the _obstaclecooldown
         * @param cooldown
         */
        void setObstacleCooldown(double cooldown);
        /**
         * Function that returns the _obstaclecooldown
         * @return
         */
        double getObstacleCooldown();

        /**
         * Function that sets the _enemycooldown
         * @param cooldown
         */
        void setEnemyCooldown(double cooldown);
        /**
         * Function that returns the _enemycooldown
         * @return
         */
        double getEnemyCooldown();

        /**
         * Function that adds a missile to the list of missiles
         * @param missile
         */
        void addMissile(shared_ptr<Missile> missile);
        /**
         * Function that sets a missile at a specific index in the missile list
         * @param missile
         * @param index
         */
        void setMissile(shared_ptr<Missile> missile, int index);
        /**
         * Function that deletes a missile at a specific index in the missile list
         * @param index
         */
        void deleteMissile(int index);
        /**
         * Function that sets the whole missile list
         * @param mlist
         */
        void setMissiles(vector<shared_ptr<Missile>> mlist);

        /**
         * Function that adds an obstacle to the obstacle list
         * @param obstacle
         */
        void addObstacle(shared_ptr<Obstacle> obstacle);
        /**
         * Function that sets an obstacle at a specific index in the obstacle list
         * @param obstacle
         * @param index
         */
        void setObstacle(shared_ptr<Obstacle> obstacle, int index);
        /**
         * Function that deletes an obstacle at a specific index in the obstacle list
         * @param index
         */
        void deleteObstacle(int index);
        /**
         * Function that sets the whole obstacle list
         * @param olist
         */
        void setObstacles(vector<shared_ptr<Obstacle>> olist);

        /**
         * Function that adds an enemy to the enemy list
         * @param enemy
         */
        void addEnemy(shared_ptr<Enemy> enemy);
        /**
         * Function that sets an enemy at a specific index in the enemy list
         * @param enemy
         * @param index
         */
        void setEnemy(shared_ptr<Enemy> enemy, int index);
        /**
         * Function that deletes an enemy at a specic index in the enemy list
         * @param index
         */
        void deleteEnemy(int index);
        /**
         * Function that sets the whole enemy list
         * @param elist
         */
        void setEnemies(vector<shared_ptr<Enemy>> elist);

        /**
         * Function that pauses (or unpauses) the game
         * @param pause
         */
        void setPause(bool pause);
        /**
         * Function that resets the world (clearing lists, resetting health...)
         * @param first
         */
        void setSoftReset(bool first = true);

    };

}

#endif //GRADIUS_WORLD_H
