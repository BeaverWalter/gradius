//
// Created by walter on 13.11.17.
//

#ifndef GRADIUS_BACKGROUND_H
#define GRADIUS_BACKGROUND_H

#include "entity.hh"

namespace gradius {

    /**
     *
     * GRADIUS::BACKGROUND Class\n
     * The class that manages all background specific details\n
     *
     */
    class Background : public Entity {

    public:

        /**
         * Default constructor for the Background class\n
         */
        Background() : Entity(0, 0, 1, 1){}
        /**
         * Constructor for the Background class that sets the position of the background
         * @param x
         * @param y
         */
        Background(double x, double y) : Entity(x, y, 1, 1) {}
        /**
         * Constructor for the Background class that sets the position and the size of the background
         * @param x
         * @param y
         * @param xs
         * @param ys
         */
        Background(double x, double y, double xs, double ys) : Entity(x, y, xs, ys, 1.0) {}
        /**
         * Constructor for the Background class that sets the position, size and speed of the background
         * @param x
         * @param y
         * @param xs
         * @param ys
         * @param speed
         */
        Background(double x, double y, double xs, double ys, double speed) : Entity(x, y, xs, ys, speed){}

        /**
         * Virtual destructor for the Background class
         */
        virtual ~Background() = default;

    };

}

#endif //GRADIUS_BACKGROUND_H
