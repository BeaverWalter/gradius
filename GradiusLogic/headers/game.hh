//
// Created by walter on 22.11.17.
//

#ifndef GRADIUS_GAME_H
#define GRADIUS_GAME_H

#include <vector>
#include <string>

#include "boost/property_tree/ptree.hpp"
#include "boost/property_tree/json_parser.hpp"

using namespace std;


namespace gradius {

    /**
     *
     * GRADIUS::GAME Class\n
     * The class that manages all game specific details, such as: the current level, the score, the level parameters...
     *
     */
    class Game {

        bool _firstload = true; /**< the bool that is true when this is the first level of the game (after starting the game, dying or resetting) */
        int _levelindex = 0; /**< the value that keeps track of the current level */
        vector<string> _levellist; /**< the value that keeps track of the levels the player can go through (and their order) */

        int _kills = 0; /**< the value that keeps track of the number of kills made */
        int _enemylimit = 0; /**< the value that keeps track of how many enemies are need to advance in the game */

        double _enemycooldown = 20.0; /**< the value that says how many times the game can try to spawn enemies per tick */
        int _enemychance = 1; /**< the value that says the chance of an enemy spawning when the game tries to spawn one (the higer, the less chance) */
        int _enemyswitchchance = 0.5;/**< the value that determines the chance that other type of enemies spawn */


        double _obstaclecooldown = 5.0; /**< the value that says how many times the game can try to spawn obstacles per tick */
        int _obstaclechance = 1; /**< the value that says the chance of an obstacle spawning when the game tries to spawn one (the higher, the less chance) */

        int _score = 0; /**< the value that keeps track of the score of the player */

        bool _endgame = false; /**< the bool that is true when the last level has ended */

    public:

        /**
         * Default constructor for the Game class
         */
        Game();

        /**
         * Function that returns the value of the _firstload attribute
         * @return
         */
        bool getFirstLoad() const;
        /**
         * Function that sets the return value of the _firstload attribute
         * @param fl
         */
        void setFirstLoad(bool fl);

        /**
         * Function that sets the _levelindex value
         * @param index
         */
        void setLevelIndex(int index);

        /**
         * Function that return the _kills value
         * @return
         */
        int getKills() const;
        /**
         * Function that sets the _kills value
         * @param kills
         */
        void setKills(int kills);

        /**
         * Function that reads which levels can be played this game from the "levels.json" file
         */
        void readLevels();
        /**
         * Function that reads the level parameters of the next level in the levellist
         * @return
         */
        bool loadNextLevel();
        /**
         * Function that reloads the current level
         */
        void reloadLevel();
        /**
         * Function that resets the game
         */
        void reloadGame();

        /**
         * Function that returns the _enemycooldown value
         * @return
         */
        double getEnemyCooldown() const;
        /**
         * Function that returns the _enemychance value
         * @return
         */
        int getEnemyChance() const;
        /**
         * Function that returns the _enemyswitchchance value
         * @return
         */
        int getEnemySwitchChance() const;

        /**
         * Function that returns the _obstaclecooldown value
         * @return
         */
        double getObstacleCooldown() const;
        /**
         * Function that returns the _obstaclechance value
         * @return
         */
        int getObstacleChance() const;

        /**
         * Function that checks if the end parameters for a level are met, and if the effective game has ended
         * @return
         */
        bool testLevelEnd();

        /**
         * Function that returns the _score value
         * @return
         */
        int getScore() const;
        /**
         * Function that set the _score value
         * @param score
         */
        void setScore(int score);

        /**
         * Function that returns the _endgame value
         * @return
         */
        bool getEndGame() const;
        /**
         * Function that set the _endgame value
         * @param end
         */
        void setEndGame(bool end);


    };

}

#endif //GRADIUS_GAME_H
