#ifndef SFML_CHRONOMETER_HH
#define SFML_CHRONOMETER_HH

#include <chrono>

using namespace std;

namespace util {

    /**
     *
     * GS::CHRONOMETER Class\n
     * Manages the 'deltaTime' of the game\n
     *
     */
    class Chronometer {

        chrono::system_clock _clock; /**< the clock that runs and is used to get the value for _time */
        chrono::system_clock::time_point _time; /**< the delta time value */
        double _deltaTime; /**< the value that represents the time between frames */

    public:

        /**
         * Default constructor for the Chronometer class;
         */
        Chronometer();
        /**
         * Function that returns a pointer to the static instance of the class
         * @return
         */
        //static Chronometer* instance();

        /**
         * Functions that returns the deltaTime\n
         * @return
         */
        double deltaTime() const;

        /**
         * Functions that resets the clock and refreshes the deltaTime\n
         */
        void resetClock();

    };

}

#endif //SFML_CHRONOMETER_HH