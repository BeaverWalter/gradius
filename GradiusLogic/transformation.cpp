//
// Created by walter on 23.11.16.
//

#include "transformation.hh"

using namespace util;

Transformation::Transformation() {

    width = 800;
    height = 600;

}

Transformation::Transformation(int x, int y) {

    width = x;
    height = y;

}

Vector Transformation::transform(Vector vec, bool loc) const {

    Vector temp(vec.getX(), vec.getY());

    double xmin = - _xmax;
    double xmax = _xmax;
    double ymin = - _ymax;
    double ymax = _ymax;

    double xrange = fabs(xmax-xmin);
    double yrange = fabs(ymax-ymin);

    double imgx = width;
    double imgy = height;

    double xdiff = xrange - xmax;
    double ydiff = yrange - ymax;

    double xrate;
    double yrate;

    if (loc){
        xrate = (temp.getX() + xdiff) / xrange;
        yrate = (temp.getY() + ydiff) / yrange;
    }
    else{
        xrate = (temp.getX()) / xrange;
        yrate = (temp.getY()) / yrange;
    }

    double newx = xrate*imgx;
    double newy = yrate*imgy;

    temp.setPos(newx, newy);

    return temp;
}

void Transformation::setTrans(int w, int h) {

    width = w;
    height = h;

}

void Transformation::setMax(double xm, double ym) {

    _xmax = xm;
    _ymax = ym;

}
