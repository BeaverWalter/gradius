//
// Created by walter on 16.11.17.
//


#include "missile.hh"

using namespace gradius;

bool Missile::checkColIn(Entity* entity){
    
    Vector position = this->getLocation();
    Vector collider = entity->getLocation();
    double w = entity->getSize().getX();
    double h = entity->getSize().getY();

    if (position.getX() + this->getSize().getX() <= collider.getX() or position.getX() >= collider.getX() + w){

        return true;

    }
    if (position.getY() + this->getSize().getY() <= collider.getY() or position.getY() >= collider.getY() + h){

        return true;

    }

    return false;

}

bool Missile::checkColOut(Entity* entity) {

    Vector pos = this->getLocation();
    Vector collider = entity->getLocation();

    Vector size = this->getSize();
    double w = entity->getSize().getX();
    double h = entity->getSize().getY();

    if (pos.getX() < collider.getX() + w &&
        pos.getX() + size.getX() > collider.getX() &&
        pos.getY() < collider.getY() + h &&
        pos.getY() + size.getY() > collider.getY()) {

        return true;
    }

    return false;
}