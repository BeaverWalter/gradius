//
// Created by walter on 20.11.17.
//

#include "enemy.hh"

using namespace gradius;

void Enemy::configure(int type, int health, double offset, double followSpeed) {

    _type = (Enemy::Type)type;
    _health = health;
    _offset = offset;
    _followspeed = followSpeed;

}


void Enemy::setHealth(int health) {
    _health = health;
}


int Enemy::getHealth() const {
    return _health;
}

double Enemy::getOffset() const {
    return _offset;
}

double Enemy::getFollowSpeed() const {
    return _followspeed;
}

gradius::Enemy::Type Enemy::getType() const {
    return _type;
}

bool Enemy::checkColOut(Entity* entity) {

    Vector pos = this->getLocation();
    Vector collider = entity->getLocation();

    Vector size = this->getSize();
    double w = entity->getSize().getX();
    double h = entity->getSize().getY();

    if (pos.getX() < collider.getX() + w &&
        pos.getX() + size.getX() > collider.getX() &&
        pos.getY() < collider.getY() + h &&
        pos.getY() + size.getY() > collider.getY()) {

        return true;
    }

    return false;
}
