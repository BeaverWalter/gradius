//
// Created by walter on 17.11.17.
//

#include "wall.hh"

using namespace gradius;

void Wall::configure(bool top) {
    _top = top;
}

bool Wall::getTop() const {
    return _top;
}