//
// Created by walter on 15.11.17.
//

#ifndef MODEL_H
#define MODEL_H

#include <world.hh>
#include "view.hh"

namespace gs {

    class View;

    /**
     *
     * GS::MODEL Class\n
     * The class that acts as 'Model' in the MVC pattern and Observable in the observer pattern\n
     * Inherits from Gradius::World
     *
     */
    class Model : public gradius::World {

        View* _observer; /**< the observer of the object */

        bool _updated = false; /**< bool which is true when the model has been updated by the controller */

    public:

        /**
         * Default Constructor for the Model class
         */
        Model() : World(0, 0, 1, 1){}
        /**
         * Constructor for the Model class which sets the position
         * @param x
         * @param y
         */
        Model(double x, double y) : World(x, y, 1, 1) {}
        /**
         * Constructor for the Model class which sets the position and the size
         * @param x
         * @param y
         * @param xs
         * @param ys
         */
        Model(double x, double y, double xs, double ys) : World(x, y, xs, ys, 1.0) {}
        /**
         * Constructor for the Model class which sets the position, size and speed
         * @param x
         * @param y
         * @param xs
         * @param ys
         * @param speed
         */
        Model(double x, double y, double xs, double ys, double speed) : World(x, y, xs, ys, speed){}

        /**
         * Virtual default destructor
         */
        virtual ~Model() = default;

        /**
         * Function that returns the observer of the object
         * @return
         */
        View* getObserver();
        /**
         * Function that sets the observer of the object
         * @param observer
         */
        void setObserver(View* observer);

        /**
         * Function that returns if the object has been updated
         * @return
         */
        bool getUpdated() const;
        /**
         * Function that sets the _updated value
         * @param update
         */
        void setUpdated(bool update);
};

}


#endif //GRADIUS_MODEL_H
