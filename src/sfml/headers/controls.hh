#ifndef SFML_CONTROLS_HH
#define SFML_CONTROLS_HH

#include <SFML/Window.hpp>
#include <iostream>
#include "fstream"

#include "boost/property_tree/ptree.hpp"
#include "boost/property_tree/json_parser.hpp"

using namespace std;

namespace gs{

    /**
     *
     * GS::CONTROLS Class\n
     * Handles sfml specific settings (and thus controls)\n
     *
     */
    class Controls{

public:

        /**
         *
         * Function that creates an instance of the transformation if none exists\n
         *
         * @return
         */
        static Controls* instance();

        /**
         *
         * Function that reads all the control settings from the controls file\n
         *
         * @param filename
         */
        void readControls(string filename);

        /**
         * Function that returns the key for moving the up\n
         * @return
         */
        sf::Keyboard::Key getUp() const;

        /**
         * Function that returns the key for moving the player down\n
         * @return
         */
        sf::Keyboard::Key getDown() const;

        /**
         * Function that returns the key for moving the player left\n
         * @return
         */
        sf::Keyboard::Key getLeft() const;

        /**
         * Function that returns the key for moving the player right\n
         * @return
         */
        sf::Keyboard::Key getRight() const;

        /**
         * Function that returns the key for pausing the game\n
         * @return
         */
        sf::Keyboard::Key getPause() const;

        /**
         * Function that returns the key for launching a missile\n
         * @return
         */
        sf::Keyboard::Key getFire() const;

        /**
         * Function that returns the key for confirmation\n
         * @return
         */
        sf::Keyboard::Key getConfirm() const;

        /**
         * Function that returns the key for resetting\n
         * @return
         */
        sf::Keyboard::Key getReset() const;

private:

        Controls();

        static Controls* m_pInstance;

        sf::Keyboard::Key _up; /** < keycode for the player to move up */
        sf::Keyboard::Key _down; /** < keycode for the player to move down */
        sf::Keyboard::Key _left; /** < keycode for the player to move left */
        sf::Keyboard::Key _right; /** < keycode for the player to move right */

        sf::Keyboard::Key _pause; /** < keycode for the game to pauze */
        sf::Keyboard::Key _fire; /** < keycode for the player to fire */
        sf::Keyboard::Key _confirm; /** < keycode for the player to confirm */
        sf::Keyboard::Key _reset; /** < keycode for the player to reset */

    };

}

#endif //SFML_CONTROLS_HH