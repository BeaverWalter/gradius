//
// Created by walter on 13.11.17.
//

#ifndef CONTROLLER_HH
#define CONTROLLER_HH

#include <math.h>
#include <memory>

#include <SFML/Graphics.hpp>

#include <settings.hh>
#include <chronometer.hh>
#include <random.hh>

#include "model.hh"

#include <game.hh>
#include <missile.hh>

namespace gs {

    /**
     *
     * GS::CONTROLLER Class\n
     * Controller Class in the MVC Design Pattern\n
     *
     */
    class Controller {

        Model* _world; /**< the model which the controller controls */
        gradius::Game* _game; /**< the state of the current game (level parameters) */

        util::Chronometer* _time; /**< the chronometer which gives the correct delta time to controler entities */
        sf::RenderWindow* _window; /**< the renderwindow used to get window events */

    public:

        /**
         *
         * Function that executes all controller functionality
         *
         */
        void run();

        /**
         * Function that controls all Game specific elements\n
         * This is: loading the current level, loading the next level, resetting the level...\n
         */
        void controlGame() const;

        /**
         * Function that controls all Player specific elements
         */
        void controlPlayer() const;

        /**
         * Function that handles all 'general inputs'\n
         * Uses the window to poll events like 'keypressed'\n
         */
        void testGeneralInput() const;

        /**
         * Function that controls all Background specific elements
         */
        void controlBackground() const;

        /**
         * Function that controls all Wall Specific elements
         */
        void controlWalls() const;

        /**
         * Function that controls all Missile specific elements
         */
        void controlMissiles() const;

        /**
         * Function that controls all Obstacle specific elements
         */
        void controlObstacles() const;

        /**
         * Function that controls all Enemy specific elements
         */
        void controlEnemies() const;

        /**
         * Function that sets all needed attributes for this class\n
         *
         * @param world
         * @param game
         * @param clock
         * @param window
         */
        void configure(Model* world, gradius::Game* game, util::Chronometer* clock, sf::RenderWindow* window);

    };

}


#endif //GRADIUS_CONTROLLER_H
