//
// Created by walter on 13.11.17.
//

#ifndef VIEW_HH
#define VIEW_HH

#include <vector>
#include <iostream>

#include <SFML/Graphics.hpp>

#include <vector.hh>
#include <chronometer.hh>

#include <world.hh>
#include <game.hh>

#include "model.hh"

namespace gs {

    class Model;

    /**
     *
     * GS::VIEW Class\n
     * View Class in the MVC Design Pattern and Observer in the Observer pattern\n
     *
     */
    class View {

        struct Textures {

            sf::Texture player;
            sf::Texture background;
            sf::Texture missile;

            sf::Texture walltop;
            sf::Texture wallbottom;

            sf::Texture obstacledown_a;
            sf::Texture obstacledown_b;
            sf::Texture obstacleup_a;
            sf::Texture obstacleup_b;

            sf::Texture enemy_normal;
            sf::Texture enemy_sick;
            sf::Texture enemy_speedy;
            sf::Texture enemy_heavy;

            sf::Texture lost;
            sf::Texture logo;

        } _textures; /**< sturct that holds all of the textures */

        struct Texts {

            sf::Text score;
            sf::Text health;
            sf::Text levelend;
            sf::Text pause;
            sf::Text gameover;
            sf::Text gameendgood;
            sf::Text gameendbad;

        } _text; /**< struct that holds all of the texts that are static */

        sf::RenderWindow _window; /**< render window of the game */
        vector<sf::RectangleShape> _shapes; /**< list with shapes to render */
        vector<sf::Text> _texts; /**< list with texts to render */
        sf::Font _font; /**< font for the texts */

        gs::Model* _world; /**< observable for the view (observer) */
        gradius::Game* _game; /**< game which stores things as score */
        util::Chronometer* _time; /**< chronometer class used to correctly render frames */

        bool _first = true; /**< bool which is true only if the game is starting as to display the logo */

        /**
         * function used to render entities (create shapes to draw)
         * @param entity
         */
        void renderEntity(gradius::Entity* entity);
        /**
         * function used to render ui elements (texts)
         */
        void renderUI();
        /**
         * function used to render the logo
         */
        void renderLogo();

        /**
         * Function used to find the right texture for the right entity
         * @param entity
         * @return
         */
        sf::Texture* resolveTexture(gradius::Entity* entity);
        /**
         * Function used to find the right text for the right ui element
         * @param guitext
         * @param text
         * @param size
         * @param color
         */
        void resolveText(sf::Text* guitext, string text, double size, int color);

        /**
         * Function used to initially load all textures
         */
        void loadTextures();
        /**
         * Functtion used to initially load all texts
         */
        void loadTexts();

    public:

        /**
         * Default constructor for the View class
         */
        View() : View(800, 600) {}
        /**
         * Constructor for the View class which sets the resolution
         * @param x
         * @param y
         */
        View(int x, int y);

        /**
         * Default destructor
         */
        ~View() = default;

        /**
         * Function thats configures additional parameters for the view
         * @param world
         * @param game
         * @param time
         */
        void configure(Model* world, gradius::Game* game, util::Chronometer* time);

        /**
         * Function that renders everything (using private methods)
         */
        void render();

        /**
         * Function that draws everything
         */
        void draw();

        /**
         * Function that returns a pointer to the renderwindow (as to help the controller detect events)
         * @return
         */
        sf::RenderWindow* getWindow();

    };

}

#endif //VIEW_H
