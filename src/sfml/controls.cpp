
#include "headers/controls.hh"

namespace pt = boost::property_tree;

using namespace gs;
using namespace std;

Controls* Controls::m_pInstance = nullptr;

Controls::Controls(){

    _up = sf::Keyboard::Key::Z;
    _down = sf::Keyboard::Key::S;
    _left = sf::Keyboard::Key::Q;
    _right = sf::Keyboard::Key::D;

    _pause = sf::Keyboard::Key::Escape;
    _fire = sf::Keyboard::Key::Space;
    _confirm = sf::Keyboard::Key::Return;
    _reset = sf::Keyboard::Key::R;

}

Controls* Controls::instance() {


    if (!m_pInstance)   // Only allow one instance of class to be generated.
        m_pInstance = new Controls;

    return m_pInstance;

}

sf::Keyboard::Key Controls::getUp() const{
    return _up;
}

sf::Keyboard::Key Controls::getDown() const{
    return _down;
}

sf::Keyboard::Key Controls::getLeft() const{
    return _left;
}

sf::Keyboard::Key Controls::getRight() const{
    return _right;
}

sf::Keyboard::Key Controls::getPause() const{
    return _pause;
}

sf::Keyboard::Key Controls::getFire() const {
    return _fire;
}

sf::Keyboard::Key Controls::getConfirm() const {
    return _confirm;
}

sf::Keyboard::Key Controls::getReset() const {
    return _reset;
}

void Controls::readControls(string filename) {

    pt::ptree settings;
    pt::read_json(filename, settings);

    string up = settings.get<string>("controls.up", "z");
    string down = settings.get<string>("controls.down", "s");
    string left = settings.get<string>("controls.left", "q");
    string right = settings.get<string>("controls.right", "d");

    string pause = settings.get<string>("controls.pause", "escape");
    string fire = settings.get<string>("controls.fire", "space");
    string confirm = settings.get<string>("controls.confirm", "return");
    string reset = settings.get<string>("controls.reset", "r");

    vector<string> controlv = {up, down, left, right, pause, fire, reset};
    vector<sf::Keyboard::Key*> keyv = {&_up, &_down, &_left, &_right, &_pause, &_fire, &_confirm, &_reset};
    for (unsigned int cstring = 0; cstring < controlv.size(); cstring++){

        string lows;

        for (char c : controlv[cstring]){
            lows += _tolower(c);
        }

        if(lows == "a"){
            *keyv[cstring] = sf::Keyboard::Key::A;
        }
        else if(lows == "b"){
            *keyv[cstring] = sf::Keyboard::Key::B;
        }
        else if(lows == "c"){
            *keyv[cstring] = sf::Keyboard::Key::C;
        }
        else if(lows == "d"){
            *keyv[cstring] = sf::Keyboard::Key::D;
        }
        else if(lows == "e"){
            *keyv[cstring] = sf::Keyboard::Key::E;
        }
        else if(lows == "f"){
            *keyv[cstring] = sf::Keyboard::Key::F;
        }
        else if(lows == "g"){
            *keyv[cstring] = sf::Keyboard::Key::G;
        }
        else if(lows == "h"){
            *keyv[cstring] = sf::Keyboard::Key::H;
        }
        else if(lows == "i"){
            *keyv[cstring] = sf::Keyboard::Key::I;
        }
        else if(lows == "j"){
            *keyv[cstring] = sf::Keyboard::Key::J;
        }
        else if(lows == "k"){
            *keyv[cstring] = sf::Keyboard::Key::K;
        }
        else if(lows == "l"){
            *keyv[cstring] = sf::Keyboard::Key::L;
        }
        else if(lows == "m"){
            *keyv[cstring] = sf::Keyboard::Key::M;
        }
        else if(lows == "n"){
            *keyv[cstring] = sf::Keyboard::Key::N;
        }
        else if(lows == "o"){
            *keyv[cstring] = sf::Keyboard::Key::O;
        }
        else if(lows == "p"){
            *keyv[cstring] = sf::Keyboard::Key::P;
        }
        else if(lows == "q"){
            *keyv[cstring] = sf::Keyboard::Key::Q;
        }
        else if(lows == "r"){
            *keyv[cstring] = sf::Keyboard::Key::R;
        }
        else if(lows == "s"){
            *keyv[cstring] = sf::Keyboard::Key::S;
        }
        else if(lows == "t"){
            *keyv[cstring] = sf::Keyboard::Key::T;
        }
        else if(lows == "u"){
            *keyv[cstring] = sf::Keyboard::Key::U;
        }
        else if(lows == "v"){
            *keyv[cstring] = sf::Keyboard::Key::V;
        }
        else if(lows == "w"){
            *keyv[cstring] = sf::Keyboard::Key::W;
        }
        else if(lows == "x"){
            *keyv[cstring] = sf::Keyboard::Key::X;
        }
        else if(lows == "y"){
            *keyv[cstring] = sf::Keyboard::Key::Y;
        }
        else if(lows == "z"){
            *keyv[cstring] = sf::Keyboard::Key::Z;
        }

        else if(lows == "left"){
            *keyv[cstring] = sf::Keyboard::Key::Left;
        }
        else if(lows == "right"){
            *keyv[cstring] = sf::Keyboard::Key::Right;
        }
        else if(lows == "up"){
            *keyv[cstring] = sf::Keyboard::Key::Up;
        }
        else if(lows == "down"){
            *keyv[cstring] = sf::Keyboard::Key::Down;
        }

        else if(lows == "num0"){
            *keyv[cstring] = sf::Keyboard::Key::Num0;
        }
        else if(lows == "num1"){
            *keyv[cstring] = sf::Keyboard::Key::Num1;
        }
        else if(lows == "num2"){
            *keyv[cstring] = sf::Keyboard::Key::Num2;
        }
        else if(lows == "num3"){
            *keyv[cstring] = sf::Keyboard::Key::Num3;
        }
        else if(lows == "num4"){
            *keyv[cstring] = sf::Keyboard::Key::Num4;
        }
        else if(lows == "num5"){
            *keyv[cstring] = sf::Keyboard::Key::Num5;
        }
        else if(lows == "num6"){
            *keyv[cstring] = sf::Keyboard::Key::Num6;
        }
        else if(lows == "num7"){
            *keyv[cstring] = sf::Keyboard::Key::Num7;
        }
        else if(lows == "num8"){
            *keyv[cstring] = sf::Keyboard::Key::Num8;
        }
        else if(lows == "num9"){
            *keyv[cstring] = sf::Keyboard::Key::Num9;
        }

        else if(lows == "numpad0"){
            *keyv[cstring] = sf::Keyboard::Key::Numpad0;
        }
        else if(lows == "numpad1"){
            *keyv[cstring] = sf::Keyboard::Key::Numpad1;
        }
        else if(lows == "numpad2"){
            *keyv[cstring] = sf::Keyboard::Key::Numpad2;
        }
        else if(lows == "numpad3"){
            *keyv[cstring] = sf::Keyboard::Key::Numpad3;
        }
        else if(lows == "numpad4"){
            *keyv[cstring] = sf::Keyboard::Key::Numpad4;
        }
        else if(lows == "numpad5"){
            *keyv[cstring] = sf::Keyboard::Key::Numpad5;
        }
        else if(lows == "numpad6"){
            *keyv[cstring] = sf::Keyboard::Key::Numpad6;
        }
        else if(lows == "numpad7"){
            *keyv[cstring] = sf::Keyboard::Key::Numpad7;
        }
        else if(lows == "numpad8"){
            *keyv[cstring] = sf::Keyboard::Key::Numpad8;
        }
        else if(lows == "numpad9"){
            *keyv[cstring] = sf::Keyboard::Key::Numpad9;
        }

        else if(lows == "space"){
            *keyv[cstring] = sf::Keyboard::Key::Space;
        }
        else if(lows == "escape"){
            *keyv[cstring] = sf::Keyboard::Key::Escape;
        }
        else if(lows == "backspace"){
            *keyv[cstring] = sf::Keyboard::Key::BackSpace;
        }
        else if(lows == "return"){
            *keyv[cstring] = sf::Keyboard::Key::Return;
        }
    }

}
