//
// Created by walter on 13.11.17.
//

#include <random.hh>
#include <settings.hh>
#include <singleton.hh>

#include "headers/view.hh"

using namespace std;
using namespace gs;

View::View(int x, int y) {

    _window.create(sf::VideoMode(x,y), "Gradius");
    _font.loadFromFile(util::Settings::instance()->getFontFile());
    this->loadTextures();
    this->loadTexts();


}

void View::configure(Model* world, gradius::Game* game, util::Chronometer* time) {

    _world = world;
    _game = game;
    _time = time;

    _world->setUpdated(true);



}

void View::draw() {

    if (_world->getUpdated()) {

        _window.clear();

        this->render();

        for (unsigned int i = 0; i < _shapes.size(); i++) {

            _window.draw(_shapes[i]);
        }


        for (unsigned int i = 0; i < _texts.size(); i++) {

            _window.draw(_texts[i]);
        }

        _window.display();

        _shapes.clear();
        _texts.clear();

        _world->setUpdated(false);

    }

    _time->resetClock();

}

sf::RenderWindow* View::getWindow() {

    return &_window;

}

void View::render() {

    renderEntity(_world->getBackground().get());
    renderEntity(_world->getPlayer().get());

    for (int mss = 0; mss < _world->getMissiles().size(); mss++) {
        if (_world->getMissiles()[mss] != nullptr) {
            gradius::Entity* entity = _world->getMissiles()[mss].get();
            renderEntity(entity);
        }
    }

    for (int ene = 0; ene < _world->getEnemies().size(); ene++) {
        if (_world->getEnemies()[ene] != nullptr) {
            gradius::Entity* entity = _world->getEnemies()[ene].get();
            renderEntity(entity);
        }
    }

    for (int obs = 0; obs < _world->getObstacles().size(); obs++) {
        if (_world->getObstacles()[obs] != nullptr) {
            gradius::Entity* entity = _world->getObstacles()[obs].get();
            renderEntity(entity);
        }
    }

    renderEntity(_world->getWalls().first.get());
    renderEntity(_world->getWalls().second.get());

    if(!_first)
        renderUI();

    renderLogo();

}

void View::renderEntity(gradius::Entity* entity) {

    if(entity != nullptr) {

        Vector ppos = entity->getLocation();
        Vector psize = entity->getSize();
        
        util::Transformation* transfo = util::Singleton<util::Transformation>::instance();

        Vector gamepos = Vector(transfo->transform(ppos));
        Vector gamesize = Vector(transfo->transform(psize, false));

        sf::RectangleShape shape(sf::Vector2f(gamesize.getX(), gamesize.getY()));
        shape.setPosition(sf::Vector2f(gamepos.getX(), gamepos.getY()));

        sf::Texture *text = this->resolveTexture(entity);
        shape.setTexture(text);

        if (gradius::Player* player = dynamic_cast<gradius::Player *>(entity) ) {

            if (player->getHitCooldown() < 10.0) {

                shape.setFillColor(sf::Color(255, 255, 255, 128));

            }

        }

        if (gradius::Background* background = dynamic_cast<gradius::Background *>(entity) ) {

            Vector size = background->getSize();
            Vector newsize = transfo->transform(size, false);
            shape.setTextureRect({0, 0, newsize.getX()/5, newsize.getY()/5});

        }

        if (gradius::Wall* wall = dynamic_cast<gradius::Wall *>(entity) ) {

            Vector size = wall->getSize();
            Vector newsize = transfo->transform(size, false);
            shape.setTextureRect({0, 0, newsize.getX()/2.5, newsize.getY()/2.5});

        }

        _shapes.push_back(shape);

    }

}

void View::renderUI() {

    //score text
    string score = "Score: " + to_string(_game->getScore());
    resolveText(&_text.score, score, 24, 2);
    _texts.push_back(_text.score);

    //health text
    string health = "Lives: " + to_string(_world->getPlayer()->getHealth());
    resolveText(&_text.health, health, 24, 1);
    _texts.push_back(_text.health);

    if (_world->getGamestate()->paused and !_game->testLevelEnd() and ! _game->getEndGame()) {

        //paused text
        _texts.push_back(_text.pause);

    }

    if (_game->testLevelEnd() and ! _game->getEndGame()) {

        //level complete text
        _texts.push_back(_text.levelend);

    }

    if (_game->getEndGame()) {

        //game over text
        _texts.push_back(_text.gameover);


        if (_world->getPlayer()->getHealth() >= 0) {

            //game complete positive text
            _texts.push_back(_text.gameendgood);

        }
        else {

            //game complete negative text
            _texts.push_back(_text.gameendbad);

        }
    }
}

void View::renderLogo() {

    if (_first) {

        _first = false;
        util::Transformation* transfo = util::Singleton<util::Transformation>::instance();

        Vector gamepos = Vector(transfo->transform(Vector(-4, -3)));
        Vector gamesize = Vector(transfo->transform(Vector(8, 6), false));
        sf::RectangleShape logo(sf::Vector2f(gamesize.getX(), gamesize.getY()));
        logo.setPosition(gamepos.getX(), gamepos.getY());
        logo.setTexture(&_textures.logo);

        _shapes.push_back(logo);
    }

}

sf::Texture* View::resolveTexture(gradius::Entity* entity) {

    if (gradius::Player* player = dynamic_cast<gradius::Player*>(entity)) {
        return &_textures.player;
    }

    if (gradius::Background* background = dynamic_cast<gradius::Background*>(entity)) {
        return &_textures.background;
    }

    if (gradius::Missile* missile = dynamic_cast<gradius::Missile*>(entity)) {
        return &_textures.missile;
    }

    if (gradius::Wall* wall = dynamic_cast<gradius::Wall*>(entity)) {

        if (wall->getTop())
            return &_textures.walltop;
        else
            return &_textures.wallbottom;
    }

    if (gradius::Obstacle* obstacle = dynamic_cast<gradius::Obstacle*>(entity)){

        if(obstacle->getTop()){

            if(0 == (int)obstacle->getType())
                return &_textures.obstacleup_a;
            if(1 == (int)obstacle->getType())
                return &_textures.obstacleup_b;

        }

        else if(!obstacle->getTop()){

            if(0 == (int)obstacle->getType())
                return &_textures.obstacledown_a;
            if(1 == (int)obstacle->getType())
                return &_textures.obstacledown_b;

        }
    }

    if (gradius::Enemy* enemy = dynamic_cast<gradius::Enemy*>(entity)) {
        if((int)enemy->getType() == 0)
            return &_textures.enemy_normal;

        if((int)enemy->getType() == 1)
            return &_textures.enemy_sick;

        if((int)enemy->getType() == 2)
            return &_textures.enemy_speedy;

        if((int)enemy->getType() == 3)
            return &_textures.enemy_heavy;
    }

    return &_textures.lost;

}

void View::resolveText(sf::Text* guitext, string text, double size, int color) {

    guitext->setFont(_font);
    guitext->setString(text);
    guitext->setCharacterSize(size);
    if (color == 0)
        guitext->setColor(sf::Color::White);
    else if (color == 1)
        guitext->setColor(sf::Color::Red);
    else if (color == 2)
        guitext->setColor(sf::Color(196, 0, 0));

}

void View::loadTextures() {

    string texturefile = util::Settings::instance()->getTextureFile();

    _textures.player.setSmooth(false);
    _textures.player.loadFromFile(texturefile, sf::IntRect(0, 0, 16, 16));

    _textures.background.setSmooth(false);
    _textures.background.setRepeated(true);
    _textures.background.loadFromFile(texturefile, sf::IntRect(16, 0, 16, 16));

    _textures.lost.setSmooth(false);
    _textures.lost.loadFromFile(texturefile, sf::IntRect(112, 112, 16, 16));

    _textures.missile.setSmooth(false);
    _textures.missile.loadFromFile(texturefile, sf::IntRect(32, 0, 16, 16));

    _textures.walltop.setSmooth(false);
    _textures.walltop.setRepeated(true);
    _textures.walltop.loadFromFile(texturefile, sf::IntRect(48, 0, 16, 16));

    _textures.wallbottom.setSmooth(false);
    _textures.wallbottom.setRepeated(true);
    _textures.wallbottom.loadFromFile(texturefile, sf::IntRect(64, 0, 16, 16));

    _textures.obstacleup_a.setSmooth(false);
    _textures.obstacleup_a.loadFromFile(texturefile, sf::IntRect(0, 16, 16, 16));

    _textures.obstacleup_b.setSmooth(false);
    _textures.obstacleup_b.loadFromFile(texturefile, sf::IntRect(16, 16, 16, 16));

    _textures.obstacledown_a.setSmooth(false);
    _textures.obstacledown_a.loadFromFile(texturefile, sf::IntRect(0, 32, 16, 16));

    _textures.obstacledown_b.setSmooth(false);
    _textures.obstacledown_b.loadFromFile(texturefile, sf::IntRect(16, 32, 16, 16));

    _textures.enemy_normal.setSmooth(false);
    _textures.enemy_normal.loadFromFile(texturefile, sf::IntRect(80, 0, 16, 16));

    _textures.enemy_sick.setSmooth(false);
    _textures.enemy_sick.loadFromFile(texturefile, sf::IntRect(96, 0, 16, 16));

    _textures.enemy_speedy.setSmooth(false);
    _textures.enemy_speedy.loadFromFile(texturefile, sf::IntRect(112, 0, 16, 16));

    _textures.enemy_heavy.setSmooth(false);
    _textures.enemy_heavy.loadFromFile(texturefile, sf::IntRect(80, 16, 16, 16));

    _textures.logo.setSmooth(false);
    _textures.logo.loadFromFile(texturefile, sf::IntRect(0, 68, 80, 60));


}

void View::loadTexts() {

    Vector trans;
    util::Transformation* transfo = util::Singleton<util::Transformation>::instance();

    //score
    string score = "Score: 0";
    resolveText(&_text.score, score, 24, 2);
    trans = transfo->transform(Vector(0, -3 + 0.2));
    _text.score.setOrigin(_text.score.getLocalBounds().width/2, _text.score.getLocalBounds().height/2);
    _text.score.setPosition(trans.getX(), trans.getY());

    //health
    string health = "Lives: 0";
    resolveText(&_text.health, health, 24, 1);
    trans = transfo->transform(Vector(0, 3 - 0.2));
    _text.health.setOrigin(_text.health.getLocalBounds().width/2, _text.health.getLocalBounds().height/2);
    _text.health.setPosition(trans.getX(), trans.getY());

    //level complete
    string levelend = "Level Complete! Press [CONFIRM] advance...";
    resolveText(&_text.levelend, levelend, 18, 0);
    trans = transfo->transform(Vector(0, 0));
    _text.levelend.setOrigin(_text.levelend.getLocalBounds().width/2, _text.levelend.getLocalBounds().height/2);
    _text.levelend.setPosition(trans.getX(), trans.getY());

    //level complete
    string pause = "GAME PAUSED";
    resolveText(&_text.pause, pause, 48, 0);
    trans = transfo->transform(Vector(0, 0));
    _text.pause.setOrigin(_text.pause.getLocalBounds().width/2, _text.pause.getLocalBounds().height/2);
    _text.pause.setPosition(trans.getX(), trans.getY());

    //game over
    string gameover = "GAME OVER!";
    resolveText(&_text.gameover, gameover, 48, 0);
    trans = transfo->transform(Vector(0, -1.5));
    _text.gameover.setOrigin(_text.gameover.getLocalBounds().width/2, _text.gameover.getLocalBounds().height/2);
    _text.gameover.setPosition(trans.getX(), trans.getY());

    //game end good
    string gameendgood = "Press [CONFIRM] to end the game or [RESTART] to restart";
    resolveText(&_text.gameendgood, gameendgood, 13, 0);
    trans = transfo->transform(Vector(0, 1.5));
    _text.gameendgood.setOrigin(_text.gameendgood.getLocalBounds().width/2, _text.gameendgood.getLocalBounds().height/2);
    _text.gameendgood.setPosition(trans.getX(), trans.getY());

    //game end bad
    string gameendbad = "YOU DIED! Press [CONFIRM] to end the game or [RESTART] to restart";
    resolveText(&_text.gameendbad, gameendbad, 12, 0);
    trans = transfo->transform(Vector(0, 1.5));
    _text.gameendbad.setOrigin(_text.gameendbad.getLocalBounds().width/2, _text.gameendbad.getLocalBounds().height/2);
    _text.gameendbad.setPosition(trans.getX(), trans.getY());


}
