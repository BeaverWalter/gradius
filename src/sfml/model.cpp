//
// Created by walter on 15.11.17.
//

#include "headers/model.hh"

using namespace gs;

View* Model::getObserver() {

    return _observer;

}

void Model::setObserver(View *observer) {

    _observer = observer;

}

bool Model::getUpdated() const {
    return _updated;
}

void Model::setUpdated(bool update) {
    _updated = update;
}