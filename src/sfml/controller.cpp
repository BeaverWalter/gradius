//
// Created by walter on 13.11.17.
//
#include "headers/controller.hh"
#include "headers/controls.hh"

using namespace gs;

void Controller::run() {

    this->controlGame();

    if (!_world->getGamestate()->paused) {

        this->controlBackground();
        this->controlPlayer();
        this->controlMissiles();
        this->controlWalls();
        this->controlObstacles();
        this->controlEnemies();

        _world->setUpdated(true);

    }

    this->testGeneralInput();

}

void Controller::controlGame() const {

    // test if first load

    if (_game->getFirstLoad()){

        _world->setSoftReset(false);

        _game->setFirstLoad(false);
        _game->readLevels();
        _game->loadNextLevel();

        _world->setUpdated(true);
    }

    // test if level is done

    if (_game->testLevelEnd()){
        _world->setPause(true);

        if(sf::Keyboard::isKeyPressed(Controls::instance()->getConfirm())) {
            if (_game->loadNextLevel() and !_game->getEndGame()) {

                _world->setSoftReset();

                _game->setKills(0);

                _world->setUpdated(true);
            }
        }

    }

    // test if game is done

    if (_game->getEndGame()) {
        _world->setPause(true);
    }

}

void Controller::controlPlayer() const {

    //movement

    shared_ptr<gradius::Player> player = _world->getPlayer();
    Vector current = player->getLocation();
    double playerspeed = player->getSpeed();

    Vector destination = current;

    if (sf::Keyboard::isKeyPressed(Controls::instance()->getUp()) && !(sf::Keyboard::isKeyPressed(Controls::instance()->getDown()))) {

        destination += Vector(0, -playerspeed * _time->deltaTime());

    }

    if (sf::Keyboard::isKeyPressed(Controls::instance()->getDown()) && !(sf::Keyboard::isKeyPressed(Controls::instance()->getUp()))) {

        destination += Vector(0, playerspeed * _time->deltaTime());

    }

    if (sf::Keyboard::isKeyPressed(Controls::instance()->getLeft()) && !(sf::Keyboard::isKeyPressed(Controls::instance()->getRight()))) {

        destination += Vector(-playerspeed * _time->deltaTime(), 0);

    }

    if (sf::Keyboard::isKeyPressed(Controls::instance()->getRight()) && !(sf::Keyboard::isKeyPressed(Controls::instance()->getLeft()))) {

        destination += Vector(playerspeed * _time->deltaTime(), 0);

    }

    if(destination.getX() > -4 && destination.getX() < (4 - player->getSize().getX()) && destination.getY() > -3 && destination.getY() < (3 - player->getSize().getY()))
        player->setLocation(destination);


    //firing

    if (sf::Keyboard::isKeyPressed(Controls::instance()->getFire())) {

        if(player->getFireCooldown() == 10.0) {

            player->setFireCooldown(player->getFireCooldown() - (util::Settings::instance()->getFireSpeed()) * _time->deltaTime() / util::Settings::instance()->getDifficulty());

            Vector loc = player->getLocation();
            Vector sz = player->getSize();

            shared_ptr<gradius::Missile> mp = make_shared<gradius::Missile>(loc.getX() + sz.getX(), loc.getY(), 0.4, 0.4, 6.0);
            _world->addMissile(mp);
        }
    }

    //player fire cooldown

    if (player->getFireCooldown() <= 0) {
        player->setFireCooldown(10.0);
    }
    else if (player->getFireCooldown() < 10.0) {
        player->setFireCooldown(player->getFireCooldown() - (util::Settings::instance()->getFireSpeed()) * _time->deltaTime() / util::Settings::instance()->getDifficulty());
    }

    //player hit cooldown

    if (player->getHitCooldown() <= 0) {
        player->setHitCooldown(10.0);
    }
    else if (player->getHitCooldown() < 10.0) {
        player->setHitCooldown(player->getHitCooldown() - 10.0 * _time->deltaTime());
    }


    //player collision

    Vector loc = player->getLocation();

    if(player->getHitCooldown() == 10.0){

        if (player->checkColOut(_world->getWalls().first.get()) || player->checkColOut(_world->getWalls().second.get())) {

            Vector dest(loc.getX(), 0.0 - player->getSize().getY());
            player->setLocation(dest);
            player->setHealth(player->getHealth() - 2);
            player->setHitCooldown(player->getHitCooldown() - 10.0 * _time->deltaTime());

        }

        for (unsigned int obs = 0; obs < _world->getObstacles().size(); obs++) {

            shared_ptr<gradius::Obstacle> o = _world->getObstacles()[obs];
            bool hit = false;

            if(o != nullptr){
                if (player->checkColOut(o.get())) {

                    hit = true;
                    Vector dest(loc.getX(), 0.0 - player->getSize().getY());
                    player->setLocation(dest);
                    player->setHealth(player->getHealth() - 1);
                    player->setHitCooldown(player->getHitCooldown() - 10.0 * _time->deltaTime());

                }
            }

            if(hit)
                break;
        }

        for (unsigned int ene = 0; ene < _world->getEnemies().size(); ene++) {

            shared_ptr<gradius::Enemy> e = _world->getEnemies()[ene];
            bool hit = false;

            if(e != nullptr){
                if (player->checkColOut(e.get())) {

                    hit = true;
                    player->setHealth(player->getHealth() - 1);
                    player->setHitCooldown(player->getHitCooldown() - 10.0 * _time->deltaTime());

                }
            }

            if(hit)
                break;
        }
    }


    //player health check

    if(player->getHealth() <= 0) {
        _game->setEndGame(true);
        _world->setPause(true);
    }

}

void Controller::testGeneralInput() const {

    sf::Event event;

    while (_window->pollEvent(event)) {

        if (event.type == sf::Event::Closed)
            _window->close();


        //pausing the game
        if (event.type == sf::Event::KeyPressed and event.key.code == Controls::instance()->getPause()){

            _world->setPause(!_world->getGamestate()->paused);

        }

        //ending the game
        if (_game->getEndGame() and event.type == sf::Event::KeyPressed and event.key.code == Controls::instance()->getConfirm()){

            _window->close();

        }

        //resetting the game
        if (_game->getEndGame() and event.type == sf::Event::KeyPressed and event.key.code == Controls::instance()->getReset()) {

            _game->reloadGame();

        }
    }
}

void Controller::controlBackground() const {

    //background scrolling

    double res = util::Settings::instance()->getXRes()/80.0;
    double intpart;

    if(modf(res, &intpart) == 0.0) {

        double max = util::Settings::instance()->getXMax();
        shared_ptr<gradius::Background> bg = _world->getBackground();

        Vector location = bg->getLocation();
        Vector size = bg->getSize();

        Vector newlocation = location;

        if (newlocation.getX() <= -max - size.getX() / 2) {

            newlocation.setPos(-max, newlocation.getY());
        } else {
            newlocation.setPos(newlocation.getX() - bg->getSpeed() * _time->deltaTime(),
                               newlocation.getY());
        }

        bg->setLocation(newlocation);

    }
}

void Controller::controlWalls() const {

    //wall scrolling

    double res = util::Settings::instance()->getXRes()/80.0;
    double intpart;

    if(modf(res, &intpart) == 0.0) {

        double max = util::Settings::instance()->getXMax();
        pair<shared_ptr<gradius::Wall>, shared_ptr<gradius::Wall>> wl = _world->getWalls();
        shared_ptr<gradius::Wall> wlf = wl.first;
        shared_ptr<gradius::Wall> wls = wl.second;

        Vector location = wlf->getLocation();
        Vector slocation = wls->getLocation();

        Vector size = wlf->getSize();

        Vector newlocation = location;
        Vector newslocation = slocation;

        if (newlocation.getX() <= -max - size.getX() / 2) {

            newlocation.setPos(-max, newlocation.getY());
            newslocation.setPos(-max, newslocation.getY());
        } else {
            newlocation.setPos(newlocation.getX() - wlf->getSpeed() * _time->deltaTime(),
                               newlocation.getY());
            newslocation.setPos(newslocation.getX() - wls->getSpeed() * _time->deltaTime(),
                               newslocation.getY());
        }

        wlf->setLocation(newlocation);
        wls->setLocation(newslocation);

    }

}

void Controller::controlMissiles() const {

    //missile movement

    for (unsigned int mss = 0; mss < _world->getMissiles().size(); mss++) {
        if (_world->getMissiles()[mss] != nullptr && _world->getMissiles()[mss].use_count() != 0) {

            Vector mssloc = _world->getMissiles()[mss]->getLocation();
            double mspeed = _world->getMissiles()[mss]->getSpeed();

            mssloc.setPos(mssloc.getX() + mspeed * _time->deltaTime(),mssloc.getY());

            _world->getMissiles()[mss]->setLocation(mssloc);
        }
    }

    //missile deletion / collision

    //todo:: if missile hit enemy, stop for loop

    for (unsigned int mss = 0; mss < _world->getMissiles().size(); mss++) {

        if (_world->getMissiles()[mss].get() != nullptr || _world->getMissiles()[mss].use_count() != 0) {

            int count = _world->getMissiles()[mss].use_count();
            auto ptr = _world->getMissiles()[mss].get();


            shared_ptr<gradius::Missile> m = _world->getMissiles()[mss];

            //delete if outside world
            if(m->checkColIn(_world)){
                _world->deleteMissile(mss);
            }

            for (unsigned int ene = 0; ene < _world->getEnemies().size(); ene++){
                if (_world->getEnemies()[ene].get() != nullptr || _world->getEnemies()[ene].use_count() != 0) {

                    shared_ptr<gradius::Enemy> e = _world->getEnemies()[ene];

                    if(m->checkColOut(e.get())) {
                        e->setHealth(e->getHealth() - 1);
                        _world->deleteMissile(mss);
                    }
                }
            }
        }
    }

    //missile cleanup

    if (_world->getMissiles().size() >= 10) {

        vector<shared_ptr<gradius::Missile>> mlist;
        for (unsigned int mss = 0; mss < _world->getMissiles().size(); mss++) {

            if (_world->getMissiles()[mss].get() != nullptr || _world->getMissiles()[mss].use_count() != 0) {

                mlist.push_back(_world->getMissiles()[mss]);

            }
        }

        _world->setMissiles(mlist);
    }
}

void Controller::controlObstacles() const {

    //generate obstacles

    if (_world->getObstacleCooldown() == 10.0) {

        _world->setObstacleCooldown(_world->getObstacleCooldown() - (_game->getObstacleCooldown()) * _time->deltaTime() * util::Settings::instance()->getDifficulty());

        double xmax = util::Settings::instance()->getXMax();
        double ymax = util::Settings::instance()->getYMax();

        //todo:: make this read the level file
        int randl = util::Random::instance()->randomInt()% (2 * _game->getObstacleChance()); // 2 * obstaclechance
        int randt = util::Random::instance()->randomInt()%2;

        if(randl == 0){
            shared_ptr<gradius::Obstacle> op = make_shared<gradius::Obstacle>(xmax, -ymax + 0.35, 0.8, 0.8, 0.75);
            op->configure(true, randt);
            _world->addObstacle(op);
        }
        else if(randl == 1){
            shared_ptr<gradius::Obstacle> op = make_shared<gradius::Obstacle>(xmax, ymax - 0.8 - 0.325, 0.8, 0.8, 0.75);
            op->configure(false, randt);
            _world->addObstacle(op);
        }
    }

    //move obstacles

    for (unsigned int obs = 0; obs < _world->getObstacles().size(); obs++) {
        if (_world->getObstacles()[obs].get() != nullptr || _world->getObstacles()[obs].use_count() != 0) {

            Vector obsloc = _world->getObstacles()[obs]->getLocation();
            double obspeed = _world->getObstacles()[obs]->getSpeed();

            obsloc.setPos(obsloc.getX()- obspeed * _time->deltaTime(),obsloc.getY());

            _world->getObstacles()[obs]->setLocation(obsloc);
        }
    }

    //obstacle deletion

    for (unsigned int obs = 0; obs < _world->getObstacles().size(); obs++) {

        if (_world->getObstacles()[obs].get() != nullptr || _world->getObstacles()[obs].use_count() != 0) {

            shared_ptr<gradius::Obstacle> o = _world->getObstacles()[obs];

            //delete if outside world
            if (o->getLocation().getX() + o->getSize().getX() < -util::Settings::instance()->getXMax()) {
                _world->deleteObstacle(obs);
            }
        }
    }

    //obstacle cleanup

    if (_world->getObstacles().size() >= 20) {

        vector<shared_ptr<gradius::Obstacle>> olist;
        for (unsigned int obs = 0; obs < _world->getObstacles().size(); obs++) {

            if (_world->getObstacles()[obs].get() != nullptr || _world->getObstacles()[obs].use_count() != 0) {

                olist.push_back(_world->getObstacles()[obs]);
            }
        }

        _world->setObstacles(olist);
    }

    if (_world->getObstacleCooldown() <= 0) {
        _world->setObstacleCooldown(10.0);
    }
    else if (_world->getObstacleCooldown() < 10.0) {
        _world->setObstacleCooldown(_world->getObstacleCooldown() - (_game->getObstacleCooldown()) * _time->deltaTime() * util::Settings::instance()->getDifficulty());
    }
}

void Controller::controlEnemies() const {

    //generate enemies

    if (_world->getEnemyCooldown() == 10.0) {

        _world->setEnemyCooldown(_world->getEnemyCooldown() - (_game->getEnemyCooldown()) * _time->deltaTime() * util::Settings::instance()->getDifficulty());

        double xmax = util::Settings::instance()->getXMax();

        int randt = util::Random::instance()->randomInt()% _game->getEnemyChance();
        double rando = util::Random::instance()->randomInt();

        if(randt == 0){

            int rande = util::Random::instance()->randomInt() % _game->getEnemySwitchChance();

            shared_ptr<gradius::Enemy> en;

            if (rande == 0) {
                en = make_shared<gradius::Enemy>(xmax + fabs(sin(rando)*4), 0 - 0.2, 0.4, 0.4, 1.25);
                en->configure(0, 1 /* *util::Settings::instance()->getDifficulty()*/, rando);
            }

            else if (rande == 1 || rande == 11) {
                double ypos = rando/1000000000 * 12 - 6;
                cout << ypos << endl;
                en = make_shared<gradius::Enemy>(xmax + fabs(sin(rando)*4), ypos - 0.2, 0.4, 0.4, 2);
                en->configure(1, 2 /* *util::Settings::instance()->getDifficulty()*/, rando, 2);
            }

            else if (rande == 2 || rande == 12) {
                en = make_shared<gradius::Enemy>(xmax + fabs(sin(rando)*4), 0 - 0.2, 0.4, 0.4, 3.5);
                en->configure(2, 1 /* *util::Settings::instance()->getDifficulty()*/, rando);
            }

            else if (rande == 3) {
                cout << "spawn heavy\n";
                en = make_shared<gradius::Enemy>(xmax + fabs(sin(rando)*4), 0 - 0.2, 0.4, 0.4, 0.35);
                en->configure(3, 5 /* *util::Settings::instance()->getDifficulty()*/, rando);
            }

            else {
                en = make_shared<gradius::Enemy>(xmax + fabs(sin(rando)*4), 0 - 0.2, 0.4, 0.4, 1.25);
                en->configure(0, 1 /* *util::Settings::instance()->getDifficulty()*/, rando);
            }

            _world->addEnemy(std::move(en));
        }
    }

    //move enemies

    for (unsigned int ene = 0; ene < _world->getEnemies().size(); ene++) {
        if (_world->getEnemies()[ene].get() != nullptr || _world->getEnemies()[ene].use_count() != 0) {

            shared_ptr<gradius::Enemy> ent = _world->getEnemies()[ene];

            double ymax = util::Settings::instance()->getYMax();

            Vector eneloc = ent->getLocation();
            double enepeed = ent->getSpeed();

            if (ent->getType() == 0)
                eneloc.setPos(eneloc.getX() - enepeed * _time->deltaTime(), (sin(eneloc.getX()*2 + fabs(sin(ent->getOffset()))*4) * (ymax - 1.6) - 0.2));

            else if (ent->getType() == 1) {
                double temp = (_world->getPlayer()->getLocation().getY() - ent->getLocation().getY()) * ent->getFollowSpeed() * _time->deltaTime();

                eneloc.setPos(eneloc.getX() - enepeed * _time->deltaTime(), ent->getLocation().getY() + temp );
            }

            else
                eneloc.setPos(eneloc.getX() - enepeed * _time->deltaTime(), (sin(eneloc.getX()*2 + fabs(sin(ent->getOffset()))*4) * (ymax - 1.6) - 0.2));


            ent->setLocation(eneloc);
        }
    }
    //enemy deletion

    for (unsigned int ene = 0; ene < _world->getEnemies().size(); ene++) {

        if (_world->getEnemies()[ene].get() != nullptr || _world->getEnemies()[ene].use_count() != 0) {

            shared_ptr<gradius::Enemy> e = _world->getEnemies()[ene];

            //delete if outside world
            if (e->getLocation().getX() + e->getSize().getX() < -util::Settings::instance()->getXMax()) {
                _world->deleteEnemy(ene);
            }

            if(e->getHealth() <= 0){
                _world->deleteEnemy(ene);

                _game->setScore(_game->getScore() + 5);
                _game->setKills(_game->getKills() + 1);
            }
        }
    }

    //enemy cleanup

    if (_world->getEnemies().size() >= _game->getEnemyCooldown()) {

        vector<shared_ptr<gradius::Enemy>> elist;
        for (unsigned int ene = 0; ene < _world->getEnemies().size(); ene++) {

            if (_world->getEnemies()[ene].get() != nullptr || _world->getEnemies()[ene].use_count() != 0) {

                elist.push_back(_world->getEnemies()[ene]);
            }
        }

        _world->setEnemies(elist);
    }

    //enemy cooldown

    if (_world->getEnemyCooldown() <= 0) {
        _world->setEnemyCooldown(10.0);
    }
    else if (_world->getEnemyCooldown() < 10.0) {
        _world->setEnemyCooldown(_world->getEnemyCooldown() - (_game->getEnemyCooldown()) * _time->deltaTime() * util::Settings::instance()->getDifficulty());
    }
}

void Controller::configure(Model* world, gradius::Game* game, util::Chronometer *clock, sf::RenderWindow* window) {

    _world = world;
    _game = game;
    _time = clock;
    _window = window;

}
