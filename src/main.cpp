#include <iostream>

#include <transformation.hh>
#include <settings.hh>
#include <world.hh>
#include <game.hh>
#include <singleton.hh>

#include "sfml/headers/controls.hh"
#include "sfml/headers/view.hh"
#include "sfml/headers/controller.hh"


int main() {

    util::Settings::instance()->readSettings("src/settings/settings.json");

    util::Transformation* transfo = util::Singleton<util::Transformation>::instance();

    transfo->setTrans(util::Settings::instance()->getXRes(), util::Settings::instance()->getYRes());
    transfo->setMax(util::Settings::instance()->getXMax(), util::Settings::instance()->getYMax());

    gs::Controls::instance()->readControls("src/settings/settings.json");

    util::Chronometer* chrono = util::Singleton<util::Chronometer>::instance();

    gs::Model world(-util::Settings::instance()->getXMax(), -util::Settings::instance()->getYMax(), util::Settings::instance()->getXMax()*2, util::Settings::instance()->getYMax()*2);

    gradius::Game game;

    gs::View view(util::Settings::instance()->getXRes(), util::Settings::instance()->getYRes());

    view.configure(&world, &game, chrono);

    gs::Controller controller;
    controller.configure(&world, &game, chrono, view.getWindow());

    world.setObserver(&view);

    while(view.getWindow()->isOpen()){

        controller.run();

        view.draw();

    }


    return 0;
}