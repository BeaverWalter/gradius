######################
## M.A.R.S. FIGHTER ##
######################

Welcome to M.A.R.S. Fighter, a Gradius-like game by Lander Geeraert!


STORY

"After years of trying to populate mars, the human race has successfully established a few colonies on MARS.
To be able to effectively live here for many years to come, the humans on MARS started to terraform the planet.
This has caused an ancient lifeform which was frozen in deep caverns to come back to life; beings which seem be to the previous inhabitants of MARS.
And now, they want their planet back, so they started attacking the humans. The humans initially took some losses when  a few colonies got destroyed, but they fought back.
The humans developed the Macro Arbitrary Redundant Serpent Fighter, or short: M.A.R.S. Fighter to try and destroy these foul beings.

You are a pilot of one of these M.A.R.S. Fighters; suit up and try to destroy as many Mars Serpents as you can before they break your shield and take down you and your ship!
Watch out for the rough MARS terrain, but make sure you don't fly to high, or else you fly into the terraforming clouds or the asteroids which will destroy your vessel..."


RULES

- At Logo screen, press the [PAUSE] button to begin

- You can shoot with your Fighter, hitting an enemy will destroy it!
- If you hit an enemy, you lose a life...
- There are asteroid and space rocks, if you hit them, you lose a life and you'll be bumped back to the center of the game...
- If you hit the ground of the (terraforming) clouds, you lose a life and you'll be bumped back to the center of the game...

- If your lives hit 'zero', the game has ended and your pilot has died!
- You are flying in sector on MARS (levels), killing enough enemies in each sector will make you advance to the next one
- If you can clear all sector, you will have won and the game will end.


CONTROLS

**You can edit the controls in the settings file (settings.json), but all have default values**

UP : Z
DOWN : S
LEFT : Q
RIGHT : D

FIRE : SPACE
PAUSE : ESCAPE
COMFIRM : ENTER

RESET : R

available buttons: a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z
num0, num1, num2, num3, num4, num5, num6, num7, num8, num9
numpad0, numpad1, numpad2, numpad3, numpad4, numpad5, numpad6, numpad7, numpad8, numpad9
space, escape, backspace, return
left, right, up, down


SETTINGS

!!! (FOR EVERY SETTING EXCEPT XRES, YRES AND DIFFICULTY) PLEASE ONLY EDIT THE SETTINGS IF YOU KNOW WHAT YOU ARE DOING, THE GAME IS MADE FOR THE DEFAULT SETTINGS AND IS NOT GUARANTEED TO WORK WITH ANY OTHER COMBINATION OF VALUES!!!

**You can edit certain settings in the settings file (settings.json), but all have default values**

xmax : the maximal x value from the center, both to the left and to the right
ymax : the maximal y value from the center, both up and down

xres : the x resolution of the game
yres : the y resolution of the game

difficulty : the difficulty of the game; this changes the amout of enemies, obstacles that can and do spawn and the cooldown for shooting missiles

speed : the speed of the player
health : the initial health of the player
firespeed : the cooldown to shoot missiles at difficulty 1 (difficulty zero is not allowed!)


FILES

**You can edit the texture and font files, but the game is not guaranteed to work when you use other files**

textures : the texture file used for the game
font : the font file used for the game


INSTALLATION AND COMPILING

The game can be compiled by;
 - executing the 'setup.sh' file included with the project


